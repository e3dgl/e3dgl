package e3dgl.common;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class TGALoader {
	private static final byte[] uncompressedTrueColor = new byte[] { 0, 0, 2,
			0, 0, 0, 0, 0, 0, 0, 0, 0 };
	private static final byte[] compressedTrueColor = new byte[] { 0, 0, 10, 0,
			0, 0, 0, 0, 0, 0, 0, 0 };
	private static final byte[] compressedGrayscale = new byte[] { 0, 0, 11, 0,
			0, 0, 0, 0, 0, 0, 0, 0 };
	private static final byte[] uncompressedGrayscale = new byte[] { 0, 0, 3,
			0, 0, 0, 0, 0, 0, 0, 0, 0 };

	public static BufferedImage loadTGA(byte[] bytes) throws IOException {
		InputStream in = new ByteArrayInputStream(bytes);
		byte[] header = new byte[12];

		readBuffer(in, header);

		if (Arrays.equals(uncompressedTrueColor, header)) {

			return loadUncompressedTGA(in);

		} else if (Arrays.equals(compressedTrueColor, header)) {

			return loadCompressedTGA(in);

		} else if (Arrays.equals(compressedGrayscale, header)) {

			return loadCompressedGrayscale(in);

		} else if (Arrays.equals(uncompressedGrayscale, header)) {

			return loadUncompressedGrayscale(in);

		} else {
			in.close();
			throw new IOException("TGA file be type 2, 10, 11 ");

		}
	}

	private static void readBuffer(InputStream in, byte[] buffer)
			throws IOException {
		int bytesRead = 0;
		int bytesToRead = buffer.length;
		while (bytesToRead > 0) {
			int read = in.read(buffer, bytesRead, bytesToRead);
			bytesRead += read;
			bytesToRead -= read;
		}
	}

	private static BufferedImage loadUncompressedGrayscale(InputStream in)
			throws IOException {

		byte[] header = new byte[6];
		readBuffer(in, header);

		int imageHeight = (unsignedByteToInt(header[3]) << 8)
				+ unsignedByteToInt(header[2]),

		imageWidth = (unsignedByteToInt(header[1]) << 8)
				+ unsignedByteToInt(header[0]),

		bpp = unsignedByteToInt(header[4]);

		if ((imageWidth <= 0) || (imageHeight <= 0) || (bpp != 8)) {

			throw new IOException("Invalid texture information");

		}
		int bytesPerPixel = (bpp / 8),

		imageSize = (bytesPerPixel * imageWidth * imageHeight);

		byte imageData[] = new byte[imageSize];

		readBuffer(in, imageData);

		BufferedImage bufferedImage = new BufferedImage(imageWidth,
				imageHeight, BufferedImage.TYPE_INT_ARGB);

		for (int j = 0; j < imageHeight; j++)
			for (int i = 0; i < imageWidth; i++) {
				int index = ((imageHeight - 1 - j) * imageWidth + i)
						* bytesPerPixel;
				int value = (255 & 0xFF) << 24 | (imageData[index + 0] & 0xFF);
				bufferedImage.setRGB(i, j, value);
			}
		return bufferedImage;
	}

	private static BufferedImage loadUncompressedTGA(InputStream in)
			throws IOException {

		byte[] header = new byte[6];
		readBuffer(in, header);

		int imageHeight = (unsignedByteToInt(header[3]) << 8)
				+ unsignedByteToInt(header[2]),

		imageWidth = (unsignedByteToInt(header[1]) << 8)
				+ unsignedByteToInt(header[0]),

		bpp = unsignedByteToInt(header[4]);

		if ((imageWidth <= 0) || (imageHeight <= 0)
				|| ((bpp != 24) && (bpp != 32))) {

			throw new IOException("Invalid texture information");

		}
		int bytesPerPixel = (bpp / 8),

		imageSize = (bytesPerPixel * imageWidth * imageHeight);

		byte imageData[] = new byte[imageSize];

		readBuffer(in, imageData);

		BufferedImage bufferedImage = new BufferedImage(imageWidth,
				imageHeight, BufferedImage.TYPE_INT_ARGB);

		for (int j = 0; j < imageHeight; j++)
			for (int i = 0; i < imageWidth; i++) {
				int index = ((imageHeight - 1 - j) * imageWidth + i)
						* bytesPerPixel, value = (255 & 0xFF) << 24
						| (imageData[index + 0] & 0xFF) << 16
						| (imageData[index + 1] & 0xFF) << 8
						| (imageData[index + 2] & 0xFF);
				bufferedImage.setRGB(i, j, value);
			}
		return bufferedImage;
	}

	private static BufferedImage loadCompressedGrayscale(InputStream fTGA)
			throws IOException

	{

		byte[] header = new byte[6];
		readBuffer(fTGA, header);

		int imageHeight = (unsignedByteToInt(header[3]) << 8)
				+ unsignedByteToInt(header[2]),

		imageWidth = (unsignedByteToInt(header[1]) << 8)
				+ unsignedByteToInt(header[0]),

		bpp = unsignedByteToInt(header[4]);

		if ((imageWidth <= 0) || (imageHeight <= 0) || ((bpp != 8))) {

			throw new IOException("Invalid texture information");

		}
		int bytesPerPixel = (bpp / 8),

		imageSize = (bytesPerPixel * imageWidth * imageHeight);

		byte imageData[] = new byte[imageSize];
		int pixelcount = imageHeight * imageWidth, currentbyte = 0,

		currentpixel = 0;
		byte[] colorbuffer = new byte[bytesPerPixel];

		do {
			int chunkheader = 0;
			try {
				chunkheader = unsignedByteToInt((byte) fTGA.read());
			} catch (IOException e) {
				throw new IOException("Could not read RLE header");
			}

			if (chunkheader < 128) {

				chunkheader++;

				for (short counter = 0; counter < chunkheader; counter++) {

					readBuffer(fTGA, colorbuffer);

					imageData[currentbyte] = colorbuffer[0];

					currentbyte += bytesPerPixel;

					currentpixel++;

					if (currentpixel > pixelcount) {

						throw new IOException("Too many pixels read");

					}
				}
			} else {
				chunkheader -= 127;

				readBuffer(fTGA, colorbuffer);
				for (short counter = 0; counter < chunkheader; counter++) {

					imageData[currentbyte] = colorbuffer[0];

					currentbyte += bytesPerPixel;

					currentpixel++;
					if (currentpixel > pixelcount) {

						throw new IOException("Too many pixels read");

					}
				}
			}
		} while (currentpixel < pixelcount);

		BufferedImage bufferedImage = new BufferedImage(imageWidth,
				imageHeight, BufferedImage.TYPE_INT_ARGB);

		for (int j = 0; j < imageHeight; j++)
			for (int i = 0; i < imageWidth; i++) {
				int index = ((imageHeight - 1 - j) * imageWidth + i)
						* bytesPerPixel;
				int value = (255 & 0xFF) << 24 | (imageData[index + 0] & 0xFF);
				bufferedImage.setRGB(i, j, value);
			}
		return bufferedImage;
	}

	private static BufferedImage loadCompressedTGA(InputStream fTGA)
			throws IOException

	{

		byte[] header = new byte[6];
		readBuffer(fTGA, header);

		int imageHeight = (unsignedByteToInt(header[3]) << 8)
				+ unsignedByteToInt(header[2]),

		imageWidth = (unsignedByteToInt(header[1]) << 8)
				+ unsignedByteToInt(header[0]),

		bpp = unsignedByteToInt(header[4]);

		if ((imageWidth <= 0) || (imageHeight <= 0)
				|| ((bpp != 24) && (bpp != 32))) {

			throw new IOException("Invalid texture information");

		}
		int bytesPerPixel = (bpp / 8),

		imageSize = (bytesPerPixel * imageWidth * imageHeight);

		byte imageData[] = new byte[imageSize];
		int pixelcount = imageHeight * imageWidth, currentbyte = 0,

		currentpixel = 0;
		byte[] colorbuffer = new byte[bytesPerPixel];

		do {
			int chunkheader = 0;
			try {
				chunkheader = unsignedByteToInt((byte) fTGA.read());
			} catch (IOException e) {
				throw new IOException("Could not read RLE header");
			}

			if (chunkheader < 128) {

				chunkheader++;

				for (short counter = 0; counter < chunkheader; counter++) {

					readBuffer(fTGA, colorbuffer);

					imageData[currentbyte] = colorbuffer[2];

					imageData[currentbyte + 1] = colorbuffer[1];
					imageData[currentbyte + 2] = colorbuffer[0];

					if (bytesPerPixel == 4)

						imageData[currentbyte + 3] = colorbuffer[3];
					currentbyte += bytesPerPixel;

					currentpixel++;

					if (currentpixel > pixelcount) {

						throw new IOException("Too many pixels read");

					}
				}
			} else {
				chunkheader -= 127;

				readBuffer(fTGA, colorbuffer);
				for (short counter = 0; counter < chunkheader; counter++) {

					imageData[currentbyte] = colorbuffer[2];

					imageData[currentbyte + 1] = colorbuffer[1];
					imageData[currentbyte + 2] = colorbuffer[0];

					if (bytesPerPixel == 4)
						imageData[currentbyte + 3] = colorbuffer[3];

					currentbyte += bytesPerPixel;

					currentpixel++;
					if (currentpixel > pixelcount) {

						throw new IOException("Too many pixels read");

					}
				}
			}
		} while (currentpixel < pixelcount);

		BufferedImage bufferedImage = new BufferedImage(imageWidth,
				imageHeight, BufferedImage.TYPE_INT_ARGB);

		for (int j = 0; j < imageHeight; j++)
			for (int i = 0; i < imageWidth; i++) {
				int index = ((imageHeight - 1 - j) * imageWidth + i)
						* bytesPerPixel, value = (255 & 0xFF) << 24
						| (imageData[index + 0] & 0xFF) << 16
						| (imageData[index + 1] & 0xFF) << 8
						| (imageData[index + 2] & 0xFF);
				bufferedImage.setRGB(i, j, value);
			}
		return bufferedImage;
	}

	private static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}
}