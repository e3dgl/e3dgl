package e3dgl.common;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import e3dgl.engine.model.Color4f;
import e3dgl.engine.model.PolygonalModel;
import e3dgl.geometry.Matrix;
import e3dgl.geometry.Vec2f;
import e3dgl.geometry.Vec3f;
import e3dgl.geometry.Vec4f;

public class Utils {

	public static final int KEY_UP = 38;

	public static final int KEY_DOWN = 40;

	public static final int KEY_LEFT = 37;

	public static final int KEY_RIGHT = 39;

	public final static String E3D_SCENE_EXTENSION = "e3d";

	public final static int RGBABytesPerPixel = 4;

	public final static int RGBBytesPerPixel = 3;

	private static final int redOffset, greenOffset, blueOffset, alphaOffset;

	static {
		boolean big = ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN;
		redOffset = big ? 24 : 0;
		greenOffset = big ? 16 : 8;
		blueOffset = big ? 8 : 16;
		alphaOffset = big ? 0 : 24;
	}

	public static final String RESOURCES_FOLDER_NAME = "res";

	public static final String OBJECTS_FOLDER_NAME = "objs";

	public static final String IMAGES_FOLDER_NAME = "images";

	public static final String PACKAGES_FOLDER_NAME = "packages";
	
	public static final String SHADERS_FOLDER_NAME = "shaders";
	
	public static final String FRAGMENT_FOLDER_NAME = "fragment";
	
	public static final String VERTEX_FOLDER_NAME = "vertex";

	public static final String OBJECTS_FOLDER = RESOURCES_FOLDER_NAME + File.separator + OBJECTS_FOLDER_NAME;

	public static final String IMAGES_FOLDER = RESOURCES_FOLDER_NAME + File.separator + IMAGES_FOLDER_NAME;

	public static final String PACKAGES_FOLDER = RESOURCES_FOLDER_NAME + File.separator + PACKAGES_FOLDER_NAME;
	
	public static final String SHADER_FOLDER = RESOURCES_FOLDER_NAME + File.separator + SHADERS_FOLDER_NAME;
	
	public static final String VERTEX_SHADER_FOLDER = SHADER_FOLDER + File.separator + VERTEX_FOLDER_NAME;
	
	public static final String FRAGMENT_SHADER_FOLDER = SHADER_FOLDER + File.separator + FRAGMENT_FOLDER_NAME;
	
	public final static float[] emptyRGBAColor = new float[] { 0, 0, 0, 0 };

	public final static float[] fullRGBAColor = new float[] { 1, 1, 1, 1 };

	public static int[] RGBToArray(int rgb) {
		return new int[] { redi(rgb), bluei(rgb), greeni(rgb) };
	}

	public static int redi(int rgb) {
		return rgb >> redOffset & 0xff;
	}

	public static int greeni(int rgb) {
		return rgb >> greenOffset & 0xff;
	}

	public static int bluei(int rgb) {
		return rgb >> blueOffset & 0xff;
	}

	public static String readFragmentShader(String name) {
		return readFromFile(FRAGMENT_SHADER_FOLDER + File.separator + name);
	}
	
	public static String readVertexShader(String name) {
		return readFromFile(VERTEX_SHADER_FOLDER + File.separator + name);
	}

	public static String readFromObjFile(String path) {
		return readFromFile(OBJECTS_FOLDER + File.separator + path);
	}

	public static String readFromSceneFile(String path) {
		return readFromFile(PACKAGES_FOLDER + File.separator + path + "." + E3D_SCENE_EXTENSION);
	}

	public static String readFromFile(String path) {
		FileInputStream inFile = null;
		try {
			inFile = new FileInputStream(path);
			byte[] str = new byte[inFile.available()];
			inFile.read(str);
			return new String(str);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inFile != null)
				try {
					inFile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return null;
	}

	public static byte[] readBytesFromImagesFolderFile(String path) {
		return readBytesFromFile(getImagePath(path));
	}

	private static String getImagePath(String path) {
		return IMAGES_FOLDER + File.separator + path;
	}

	public static byte[] readBytesFromFile(String path) {
		FileInputStream inFile = null;
		try {
			inFile = new FileInputStream(path);
			byte[] str = new byte[inFile.available()];
			inFile.read(str);
			return str;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inFile != null)
				try {
					inFile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return null;
	}

	public static BufferedImage readAndFlipTGAImage(String path) {
		return flipImage(readTGAImage(path));
	}

	private static BufferedImage flipImage(BufferedImage image) {
		AffineTransform at = new AffineTransform();
		at.concatenate(AffineTransform.getScaleInstance(1, -1));
		at.concatenate(AffineTransform.getTranslateInstance(0,
				-image.getHeight()));
		return createTransformed(image, at);
	}

	private static BufferedImage createTransformed(BufferedImage image, AffineTransform at) {
		BufferedImage newImage = new BufferedImage(image.getWidth(),
				image.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = newImage.createGraphics();
		g.transform(at);
		g.drawImage(image, 0, 0, null);
		g.dispose();
		return newImage;
	}

	public static BufferedImage readImage(String path) throws IOException {
		return ImageIO.read(new File(getImagePath(path)));
	}

	public static BufferedImage readTGAImage(String path) {
		try {
			return TGALoader.loadTGA(readBytesFromImagesFolderFile(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Vec4f parseVec4fFromString(String line, String delimeter) {
		String[] splitted = line.trim().split(delimeter);
		return new Vec4f(
				getSplittedFloat(0, splitted), 
				getSplittedFloat(1, splitted),
				getSplittedFloat(2, splitted),
				getSplittedFloat(3, splitted));
	}

	public static Color4f parseColor4fFromString(String line, String delimeter) {
		String[] splitted = line.trim().split(delimeter);
		return new Color4f(
				getSplittedFloat(0, splitted), 
				getSplittedFloat(1, splitted),
				getSplittedFloat(2, splitted),
				getSplittedFloat(3, splitted));
	}

	public static Vec3f parseVec3fFromString(String line, String delimeter) {
		String[] splitted = line.trim().split(delimeter);
		return new Vec3f(
				getSplittedFloat(0, splitted),
				getSplittedFloat(1, splitted),
				getSplittedFloat(2, splitted));
	}
	
	public static Vec2f parseVec2fFromString(String line, String delimeter) {
		String[] splitted = line.trim().split(delimeter);
		return new Vec2f(
				getSplittedFloat(0, splitted),
				getSplittedFloat(1, splitted));
	}
	
	public static float getSplittedFloat(int i, String splitted[]) {
		return splitted.length > i ? Float.parseFloat(splitted[i]) : 0;
	}

	public static float redf(int rgb) {
		return ((float) (rgb >> redOffset & 0xff)) / 255f;
	}

	public static float greenf(int rgb) {
		return ((float) (rgb >> greenOffset & 0xff)) / 255f;
	}

	public static float bluef(int rgb) {
		return ((float) (rgb >> blueOffset & 0xff)) / 255f;
	}

	public static void saveScene(String serializedString, String path) {
		saveStringToFile(serializedString, PACKAGES_FOLDER + File.separator + path + "." + E3D_SCENE_EXTENSION);
	}

	public static void saveStringToFile(String content, String path) {
		FileWriter writer;
		try {
			writer = new FileWriter(new File(path), false);
			writer.write(content);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Matrix getRotXMatrix(float angle) {
		float s = (float) Math.sin(angle);
		float c = (float) Math.cos(angle);

		Matrix matrix = new Matrix(3, 3);

		matrix.values[0][0] = 1;
		matrix.values[0][1] = 0;
		matrix.values[0][2] = 0;
		matrix.values[1][0] = 0;
		matrix.values[1][1] = c;
		matrix.values[1][2] = -s;
		matrix.values[2][0] = 0;
		matrix.values[2][1] = s;
		matrix.values[2][2] = c;

		return matrix;
	}

	public static Matrix getRotYMatrix(float angle) {
		float s = (float) Math.sin(angle);
		float c = (float) Math.cos(angle);

		Matrix matrix = new Matrix(3, 3);
		matrix.values[0][0] = c;
		matrix.values[0][1] = 0;
		matrix.values[0][2] = s;
		matrix.values[1][0] = 0;
		matrix.values[1][1] = 1;
		matrix.values[1][2] = 0;
		matrix.values[2][0] = -s;
		matrix.values[2][1] = 0;
		matrix.values[2][2] = c;

		return matrix;
	}

	public static Matrix getRotZMatrix(float angle) {
		float s = (float) Math.sin(angle);
		float c = (float) Math.cos(angle);

		Matrix matrix = new Matrix(3, 3);
		matrix.values[0][0] = c;
		matrix.values[0][1] = -s;
		matrix.values[0][2] = 0;
		matrix.values[1][0] = s;
		matrix.values[1][1] = c;
		matrix.values[1][2] = 0;
		matrix.values[2][0] = 0;
		matrix.values[2][1] = 0;
		matrix.values[2][2] = 1;

		return matrix;
	}

	public static BufferedImage readAndFlipImage(String path)
			throws IOException {
		return flipImage(readImage(path));
	}

	public static Matrix randomAxisRotateMatrix(Vec3f axis, float teta) {
		return randomAxisRotateMatrix(axis.x, axis.y, axis.z, teta);
	}

	/**
	 * @param x
	 *            , y, z - axis coords around which rotate will be produce
	 * @param teta
	 *            - rotation angle
	 * @return - rotation matrix
	 */
	public static Matrix randomAxisRotateMatrix(float x, float y, float z, float teta) {
		float ct = (float) Math.cos(teta);
		float st = (float) Math.sin(teta);
		float omct = 1 - ct;
		Matrix matrix = new Matrix(3, 3);
		matrix.values[0][0] = ct + omct * x * x;
		matrix.values[0][1] = omct * x * y - st * z;
		matrix.values[0][2] = omct * x * y + st * y;

		matrix.values[1][0] = omct * y * x + st * z;
		matrix.values[1][1] = ct + omct * y * y;
		matrix.values[1][2] = omct * y * z - st * x;

		matrix.values[2][0] = omct * z * x - st * y;
		matrix.values[2][1] = omct * z * y + st * x;
		matrix.values[2][2] = ct + omct * z * z;
		return matrix;
	}

	public static int generateTextureFromRGBAArray2D(int[] texture, int width, int height) {
		IntBuffer intBuffer = createIntBuffer(texture.length);
		intBuffer.put(texture);
		intBuffer.flip();

		int texId = GL11.glGenTextures();
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, texId);
		// GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, width, height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, intBuffer);
		GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
		return texId;

	}

	public static int generateTexture2D(int[][][] texture, boolean isFlipped, Map<Integer, Integer> texParameters) {
		int textureId = generateTextureFromRGBAArray2D(
				isFlipped ? convertRGBATextureToLinearIntArrayFlipped(texture)
						: convertRGBATextureToLinearIntArray(texture),
				isFlipped ? texture.length : texture[0].length,
				isFlipped ? texture[0].length : texture.length);
		applyTexParameters2D(texParameters);
		return textureId;
	}

	public static int generateTexture2DFromLinear(int[] texture, int width, int height, Map<Integer, Integer> texParameters) {
		int textureId = generateTextureFromRGBAArray2D(
				convertRGBATextureToLinearIntArray(texture), width, height);
		applyTexParameters2D(texParameters);
		return textureId;
	}

	public static int generateTexture2DStretched(int[] texture, int width, int height) {
		return generateTexture2DFromLinear(texture, width, height,
				new HashMap<Integer, Integer>() {
					private static final long serialVersionUID = 1L;
					{
						put(GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
						put(GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
					}
				});
	}

	public static int generateTexture2DStretchedSmoothed(int[] texture, int width, int height) {
		return generateTexture2DFromLinear(texture, width, height,
				new HashMap<Integer, Integer>() {
					private static final long serialVersionUID = 1L;
					{
						put(GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
						put(GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
						put(GL11.GL_TEXTURE_WRAP_T, GL11.GL_CLAMP);
					}
				});
	}

	public static int generateTexture2DStretched(int[][][] texture, boolean isFlipped) {
		return generateTexture2D(texture, isFlipped,
				new HashMap<Integer, Integer>() {
					private static final long serialVersionUID = 1L;
					{
						put(GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
						put(GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
					}
				});
	}

	public static int generateTexture2DStretchedSmoothed(int[][][] texture,
			boolean isFlipped) {
		return generateTexture2D(texture, isFlipped,
				new HashMap<Integer, Integer>() {
					private static final long serialVersionUID = 1L;
					{
						put(GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
						put(GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
						put(GL11.GL_TEXTURE_WRAP_T, GL11.GL_CLAMP);
					}
				});
	}

	public static void applyTexParameters2D(Map<Integer, Integer> texParameters) {
		for (Entry<Integer, Integer> texParameterEntry : texParameters.entrySet()) {
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, texParameterEntry.getKey(), texParameterEntry.getValue());
		}
	}

	public static IntBuffer createIntBuffer(int capacity) {
		return ByteBuffer.allocateDirect(capacity << 2)
				.order(ByteOrder.nativeOrder()).order(ByteOrder.nativeOrder()).asIntBuffer();
	}

	public static int[] convertRGBATextureToLinearIntArrayFlipped(
			int[][][] texture) {
		int[] textureArray = new int[texture.length * texture[0].length];
		for (int j = 0; j < texture[0].length; j++) {
			for (int i = 0; i < texture.length; i++) {
				textureArray[j * texture.length + i] = Utils.packInt(
					texture[i][j][0], 
					texture[i][j][1], 
					texture[i][j][2],
					texture[i][j][3]);
			}
		}
		return textureArray;
	}

	public static int[] convertRGBATextureToLinearIntArray(int[][][] texture) {
		int[] textureArray = new int[texture.length * texture[0].length];
		for (int i = 0; i < texture.length; i++) {
			for (int j = 0; j < texture[0].length; j++) {
				textureArray[i * texture[0].length + j] = Utils.packInt(
					texture[i][j][0], 
					texture[i][j][1], 
					texture[i][j][2],
					texture[i][j][3]);
			}
		}
		return textureArray;
	}

	public static int[] convertRGBATextureToLinearIntArray(int[] texture) {
		int[] textureArray = new int[texture.length / 4];
		for (int i = 0; i < textureArray.length; i++) {
			textureArray[i] = Utils.packInt(
				texture[i * 4], 
				texture[i * 4 + 1],
				texture[i * 4 + 2], 
				texture[i * 4 + 3]);
		}
		return textureArray;
	}

	public static int packInt(int r, int g, int b, int a) {
		r = (r & 0xff) << redOffset;
		g = (g & 0xff) << greenOffset;
		b = (b & 0xff) << blueOffset;
		a = (a & 0xff) << alphaOffset;
		return r | g | b | a;
	}
	
	public static int loadTexture2DStretchedSmoothed(BufferedImage image) {
		return loadTexture(image,
			new HashMap<Integer, Integer>() {
				private static final long serialVersionUID = 1L;
				{
					put(GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
					put(GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
					/*put(GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
					put(GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
					put(GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
					put(GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);*/
				}
			});
	}
	
	public static int loadTexture(BufferedImage image, Map<Integer, Integer> texParameters) {
		int textureId = loadTexture(image, Utils.RGBABytesPerPixel);
		applyTexParameters2D(texParameters);
		return textureId;
	}
	
	public static int loadTexture(BufferedImage image, int bps) {
		int[] pixels = new int[image.getWidth() * image.getHeight()];
		image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());

		ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * bps);
		for (int y = 0; y < image.getHeight(); y++) {
			for (int x = 0; x < image.getWidth(); x++) {
				int pixel = pixels[y * image.getWidth() + x];

				buffer.put((byte) (pixel & 0xFF));
				buffer.put((byte) ((pixel >> 8) & 0xFF));
				buffer.put((byte) ((pixel >> 16) & 0xFF));
				buffer.put((byte) ((pixel >> 24) & 0xFF));

				// TODO: for non tga next				
/*				buffer.put((byte) ((pixel >> 16) & 0xFF));
				buffer.put((byte) ((pixel >> 8) & 0xFF));
				buffer.put((byte) (pixel & 0xFF));
				buffer.put((byte) ((pixel >> 24) & 0xFF));*/
				
/*				
				
				buffer.put((byte) ((pixel >> 24) & 0xFF));
				buffer.put((byte) ((pixel >> 16) & 0xFF));
				buffer.put((byte) ((pixel >> 8) & 0xFF));
				buffer.put((byte) (pixel & 0xFF));
	*/			
				

			}
		}

		buffer.flip();
		int textureID = GL11.glGenTextures();		
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, image.getWidth(), image.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);
		return textureID;
	}
	
	public static void fillFromObjFile(PolygonalModel model, String path) {
		model.setObjFile(path);
		BufferedReader reader = null;
		List<Vec3f> vertexes = new ArrayList<Vec3f>();
		List<Vec3f> uvs = new ArrayList<Vec3f>();
		List<Vec3f> normals = new ArrayList<Vec3f>();
		List<int[][]> faces = new ArrayList<int[][]>();
		try {
			reader = new BufferedReader(new FileReader(Utils.OBJECTS_FOLDER
					+ File.separator + path));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.startsWith("v ")) {
					vertexes.add(Utils.parseVec3fFromString(
							line.substring(2), " "));
				} else if (line.startsWith("vt ")) {
					uvs.add(Utils.parseVec3fFromString(line.substring(3),
							" "));
				} else if (line.startsWith("vn ")) {
					normals.add(Utils.parseVec3fFromString(
							line.substring(3), " "));
				} else if (line.startsWith("f ")) {
					String[] faceVertexesStr = line.substring(2).split(" ");
					int[][] face = new int[faceVertexesStr.length][3];
					for (int i = 0; i < faceVertexesStr.length; i++) {
						int index = faceVertexesStr[i].indexOf("/");
						if (index > 0) {
							face[i][0] = Integer.parseInt(faceVertexesStr[i]
									.substring(0, index)) - 1;
							int secondIndex = faceVertexesStr[i].indexOf("/",
									index + 1);
							if (secondIndex > index + 1) {
								face[i][1] = Integer
										.parseInt(faceVertexesStr[i].substring(
												index + 1, secondIndex)) - 1;
								face[i][2] = Integer
										.parseInt(faceVertexesStr[i]
												.substring(secondIndex + 1)) - 1;
							}
						} else {
							face[i][0] = Integer.parseInt(faceVertexesStr[i]) - 1;
						}
					}
					faces.add(face);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		float[][] vertexesArray = new float[vertexes.size()][];
		for(int i=0; i<vertexes.size(); i++) {
			vertexesArray[i] = vertexes.get(i).toArray();
		}
		
		float[][] normalsArray = new float[normals.size()][];
		for(int i=0; i<normals.size(); i++) {
			normalsArray[i] = normals.get(i).toArray();
		}
		
		float[][] uvsArray = new float[uvs.size()][];
		for(int i=0; i<uvs.size(); i++) {
			uvsArray[i] = uvs.get(i).toArray();
		}
		
		int[][][] facesArray = new int[faces.size()][][];
		for(int i=0; i<faces.size(); i++) {
			int[][] face = faces.get(i);
			facesArray[i] = new int[][]{
				{face[0][0], face[1][0], face[2][0]},
				{face[0][1], face[1][1], face[2][1]},
				{face[0][2], face[1][2], face[2][2]},
				{0, 0, 0},
				{0, 0, 0}
			};
		}
		
		model.setVertexes(vertexesArray);
		model.setUVs(uvsArray);
		model.setNormals(normalsArray);
		model.setPolygons(facesArray);
	}

	public static int getGLTexID(int parseInt) {
		if(parseInt == 0)
			return GL13.GL_TEXTURE0;
		if(parseInt == 1)
			return GL13.GL_TEXTURE1;
		if(parseInt == 2)
			return GL13.GL_TEXTURE2;
		if(parseInt == 3)
			return GL13.GL_TEXTURE3;
		if(parseInt == 4)
			return GL13.GL_TEXTURE4;
		if(parseInt == 5)
			return GL13.GL_TEXTURE5;
		if(parseInt == 6)
			return GL13.GL_TEXTURE6;
		if(parseInt == 7)
			return GL13.GL_TEXTURE7;
		if(parseInt == 8)
			return GL13.GL_TEXTURE8;
		if(parseInt == 9)
			return GL13.GL_TEXTURE9;
		if(parseInt == 10)
			return GL13.GL_TEXTURE10;
		if(parseInt == 11)
			return GL13.GL_TEXTURE11;
		if(parseInt == 12)
			return GL13.GL_TEXTURE12;
		if(parseInt == 13)
			return GL13.GL_TEXTURE13;
		if(parseInt == 14)
			return GL13.GL_TEXTURE14;
		if(parseInt == 15)
			return GL13.GL_TEXTURE15;
		if(parseInt == 16)
			return GL13.GL_TEXTURE16;
		if(parseInt == 17)
			return GL13.GL_TEXTURE17;
		if(parseInt == 18)
			return GL13.GL_TEXTURE18;
		if(parseInt == 19)
			return GL13.GL_TEXTURE19;
		if(parseInt == 20)
			return GL13.GL_TEXTURE20;
		if(parseInt == 21)
			return GL13.GL_TEXTURE21;
		if(parseInt == 22)
			return GL13.GL_TEXTURE22;
		if(parseInt == 23)
			return GL13.GL_TEXTURE23;
		if(parseInt == 24)
			return GL13.GL_TEXTURE24;
		if(parseInt == 25)
			return GL13.GL_TEXTURE25;
		if(parseInt == 26)
			return GL13.GL_TEXTURE26;
		if(parseInt == 27)
			return GL13.GL_TEXTURE27;
		if(parseInt == 28)
			return GL13.GL_TEXTURE28;
		if(parseInt == 29)
			return GL13.GL_TEXTURE29;
		if(parseInt == 30)
			return GL13.GL_TEXTURE30;
		if(parseInt == 31)
			return GL13.GL_TEXTURE31;
		throw new UnsupportedOperationException("Wrong texture index");
	}
	
	public static int getIDByGLTex(int texId) {
		if(texId == GL13.GL_TEXTURE0)
			return 0;
		if(texId == GL13.GL_TEXTURE1)
			return 1;
		if(texId == GL13.GL_TEXTURE2)
			return 2;
		if(texId == GL13.GL_TEXTURE3)
			return 3;
		if(texId == GL13.GL_TEXTURE4)
			return 4;
		if(texId == GL13.GL_TEXTURE5)
			return 5;
		if(texId == GL13.GL_TEXTURE6)
			return 6;
		if(texId == GL13.GL_TEXTURE7)
			return 7;
		if(texId == GL13.GL_TEXTURE8)
			return 8;
		if(texId == GL13.GL_TEXTURE9)
			return 9;
		if(texId == GL13.GL_TEXTURE10)
			return 10;
		if(texId == GL13.GL_TEXTURE11)
			return 11;
		if(texId == GL13.GL_TEXTURE12)
			return 12;
		if(texId == GL13.GL_TEXTURE13)
			return 13;
		if(texId == GL13.GL_TEXTURE14)
			return 14;
		if(texId == GL13.GL_TEXTURE15)
			return 15;
		if(texId == GL13.GL_TEXTURE16)
			return 16;
		if(texId == GL13.GL_TEXTURE17)
			return 17;
		if(texId == GL13.GL_TEXTURE18)
			return 18;
		if(texId == GL13.GL_TEXTURE19)
			return 19;
		if(texId == GL13.GL_TEXTURE20)
			return 20;
		if(texId == GL13.GL_TEXTURE21)
			return 21;
		if(texId == GL13.GL_TEXTURE22)
			return 22;
		if(texId == GL13.GL_TEXTURE23)
			return 23;
		if(texId == GL13.GL_TEXTURE24)
			return 24;
		if(texId == GL13.GL_TEXTURE25)
			return 25;
		if(texId == GL13.GL_TEXTURE26)
			return 26;
		if(texId == GL13.GL_TEXTURE27)
			return 27;
		if(texId == GL13.GL_TEXTURE28)
			return 28;
		if(texId == GL13.GL_TEXTURE29)
			return 29;
		if(texId == GL13.GL_TEXTURE30)
			return 10;
		if(texId == GL13.GL_TEXTURE31)
			return 31;
		throw new UnsupportedOperationException("Wrong texture index");
	}
	
	public static int createShader(String filename, boolean isFragment) {
		int shaderType = isFragment ? GL20.GL_FRAGMENT_SHADER : GL20.GL_VERTEX_SHADER;
        int shader = 0;
        
            shader = GL20.glCreateShader(shaderType);
             
            if(shader == 0)
                return 0;
             
            
            GL20.glShaderSource(shader, isFragment ? readFragmentShader(filename) : readVertexShader(filename));
            GL20.glCompileShader(shader);
            
    		int comp = GL20.glGetShaderi(shader, GL20.GL_COMPILE_STATUS);
    		int len = GL20.glGetShaderi(shader, GL20.GL_INFO_LOG_LENGTH);
    		String t = shaderTypeString(shaderType);
    		String err = GL20.glGetShaderInfoLog(shader, len);
    		String log = "";
    		if (err != null && err.length() != 0)
    			log += t + " compile log:\n" + err + "\n";
    		if (comp == GL11.GL_FALSE)
				try {
					throw new LWJGLException(log.length()!=0 ? log : "Could not compile "+shaderTypeString(shaderType));
				} catch (LWJGLException e) {
					e.printStackTrace();
				}
    		return shader;        
    }
	
	public static String shaderTypeString(int type) {
		if (type == GL20.GL_FRAGMENT_SHADER)
			return "FRAGMENT_SHADER";
		else if (type == GL20.GL_VERTEX_SHADER)
			return "VERTEX_SHADER";
		else
			return "shader";
	}

	public static int getIdByLightOpenGLId(int id) {
		if(id == GL11.GL_LIGHT0)
			return 0;
		if(id == GL11.GL_LIGHT1)
			return 1;
		if(id == GL11.GL_LIGHT2)
			return 2;
		if(id == GL11.GL_LIGHT3)
			return 3;
		if(id == GL11.GL_LIGHT4)
			return 4;
		if(id == GL11.GL_LIGHT5)
			return 5;
		if(id == GL11.GL_LIGHT6)
			return 6;
		if(id == GL11.GL_LIGHT7)
			return 7;
		throw new UnsupportedOperationException("Wrong light number: " + id);	
	}
	
	public static int getLightOpenGLId(int id) {
		if(id == 0)
			return GL11.GL_LIGHT0;
		if(id == 1)
			return GL11.GL_LIGHT1;
		if(id == 2)
			return GL11.GL_LIGHT2;
		if(id == 3)
			return GL11.GL_LIGHT3;
		if(id == 4)
			return GL11.GL_LIGHT4;
		if(id == 5)
			return GL11.GL_LIGHT5;
		if(id == 6)
			return GL11.GL_LIGHT6;
		if(id == 7)
			return GL11.GL_LIGHT7;
		throw new UnsupportedOperationException("Wrong light number: " + id);
	}
}

