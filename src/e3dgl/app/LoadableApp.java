package e3dgl.app;

import e3dgl.engine.console.Console;
import e3dgl.engine.model.E3DLoadableSceneInitializer;

public class LoadableApp {

	public static void main(String[] args) {
		new Console(new Config(), new E3DLoadableSceneInitializer("scene")).start();
	}
}