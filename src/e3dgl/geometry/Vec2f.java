package e3dgl.geometry;

public class Vec2f {

	public float x, y;

	public Vec2f() {
		this(0, 0);
	}

	public Vec2f(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Vec2f(Vec3i v) {
		this.x = v.x;
		this.y = v.y;
	}

	public void set(Vec2f vector) {
		this.x = vector.x;
		this.y = vector.y;
	}

	public Vec4f toVec4f() {
		return new Vec4f(x, y, 1, 1);
	}

	public Vec2f minus(Vec2f v) {
		return new Vec2f(x - v.x, y - v.y);
	}

	public float norm() {
		return (float) Math.sqrt(x * x + y * y);
	}

	public Vec2f normalize() {
		float norm = norm();
		x = x / norm;
		y = y / norm;
		return this;
	}

	@Override
	public String toString() {
		return x + " " + y;
	}

	public Vec2f copy() {
		return new Vec2f(x, y);
	}

	public Vec2f mul(float alpha) {
		return new Vec2f(x * alpha, y * alpha);
	}

}
