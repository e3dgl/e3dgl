package e3dgl.geometry;

public class Vec3i {

	public int x, y, z;

	public Vec3i() {
	}
	
	public Vec3i(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vec3i(float x, float y, float z) {
		this.x = (int) x;
		this.y = (int) y;
		this.z = (int) z;
	}

	public void set(Vec3i vector) {
		this.x = vector.x;
		this.y = vector.y;
		this.z = vector.z;
	}

	public Vec3i minus(Vec3i v) {
		return new Vec3i(x - v.x, y - v.y, z - v.z);
	}

	public Vec3i crossProduct(Vec3i v) {
		return new Vec3i(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y
				* v.x);
	}

	public float dotProduct(Vec3i v) {
		return x * v.x + y * v.y + z * v.z;
	}

	@Override
	public String toString() {
		return x + " " + y + " " + z;
	}

	public Vec3i copy() {
		return new Vec3i(x, y, z);
	}

	public Vec3i plus(Vec3f v) {
		return new Vec3i(x + v.x, y + v.y, z + v.z);
	}

	public Vec3f toVec3f() {
		return new Vec3f(x, y, z);
	}

}
