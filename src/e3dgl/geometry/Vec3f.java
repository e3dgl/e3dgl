package e3dgl.geometry;

import java.text.DecimalFormat;

import e3dgl.common.Utils;

public class Vec3f {

	public float x, y, z;

	private static Matrix ms41 = new Matrix(4, 1);

	private static Matrix ms31 = new Matrix(3, 1);

	private static Vec4f v4fs = new Vec4f();

	private static DecimalFormat format = new DecimalFormat("#.##");

	public Vec3f() {
		this(0, 0, 0);
	}

	public Vec3f(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vec3f(Vec3i v) {
		this.x = v.x;
		this.y = v.y;
		this.z = v.z;
	}

	public void set(Vec3f vector) {
		this.x = vector.x;
		this.y = vector.y;
		this.z = vector.z;
	}

	public Vec3f minus(Vec3f v) {
		return new Vec3f(x - v.x, y - v.y, z - v.z);
	}

	public Vec3f _minus(Vec3f v) {
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return this;
	}

	public Vec3f crossProduct(Vec3f v) {
		return new Vec3f(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y
				* v.x);
	}

	public Vec3f crossProductSingleton(Vec3f v) {
		float tmpx = x;
		float tmpy = y;
		float tmpz = z;
		x = tmpy * v.z - tmpz * v.y;
		y = tmpz * v.x - tmpx * v.z;
		z = tmpx * v.y - tmpy * v.x;
		return this;
	}

	public float norm() {
		return (float) Math.sqrt(x * x + y * y + z * z);
	}

	public Vec3f normalize() {
		float norm = norm();
		x = x / norm;
		y = y / norm;
		z = z / norm;
		return this;
	}

	public float dotProduct(Vec3f v) {
		return x * v.x + y * v.y + z * v.z;
	}

	@Override
	public String toString() {
		return format.format(x) + " " + format.format(y) + " "
				+ format.format(z);
	}

	public Vec3f copy() {
		return new Vec3f(x, y, z);
	}

	public Vec3f mul(float alpha) {
		return new Vec3f(x * alpha, y * alpha, z * alpha);
	}

	public Vec3f plus(Vec3f v) {
		return new Vec3f(x + v.x, y + v.y, z + v.z);
	}

	public Vec3f _plus(Vec3f v) {
		x += v.x;
		y += v.y;
		z += v.z;
		return this;
	}

	public Matrix toMatrix4Singleton() {
		ms41.values[0][0] = x;
		ms41.values[1][0] = y;
		ms41.values[2][0] = z;
		ms41.values[3][0] = 1;
		return ms41;
	}

	public Matrix toMatrix4() {
		Matrix m = new Matrix(4, 1);
		m.values[0][0] = x;
		m.values[1][0] = y;
		m.values[2][0] = z;
		m.values[3][0] = 1;
		return m;
	}

	public Matrix toMatrixSingleton() {
		ms31.values[0][0] = x;
		ms31.values[1][0] = y;
		ms31.values[2][0] = z;
		return ms31;
	}

	public Vec4f toVec4f() {
		return new Vec4f(this.x, this.y, this.z, 1);
	}

	public Vec4f toVec4fSingleton() {
		v4fs.x = x;
		v4fs.y = y;
		v4fs.z = z;
		v4fs.i = 1;
		return v4fs;
	}

	public Vec2f toVec2f() {
		return new Vec2f(this.x, this.y);
	}

	public float scalarProduct(float[] varying_intensity) {
		return x * varying_intensity[0] + y * varying_intensity[1] + z
				* varying_intensity[2];
	}

	public Vec3i toVec3i() {
		return new Vec3i(x, y, z);
	}

	public static Vec3f parse(String line) {
		return Utils.parseVec3fFromString(line, " ");
	}

	public Vec3f div(float div) {
		return new Vec3f(x / div, y / div, z / div);
	}

	public Vec3f _div(float div) {
		x /= div;
		y /= div;
		z /= div;
		return this;
	}

	public float[] toArray() {
		return new float[] { x, y, z };
	}

	public float[] toArray4() {
		return new float[] { x, y, z, 0 };
	}

}
