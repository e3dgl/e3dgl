package e3dgl.geometry;

import e3dgl.common.Utils;

public class Vec4f {

	public float x, y, z, i;

	private static Matrix ms41 = new Matrix(4, 1);

	public Vec4f() {
		this(0, 0, 0, 0);
	}

	public Vec4f(float x, float y, float z, float i) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.i = i;
	}

	public Matrix toMatrixSingleton() {
		ms41.values[0][0] = x;
		ms41.values[1][0] = y;
		ms41.values[2][0] = z;
		ms41.values[3][0] = i;
		return ms41;
	}

	public Matrix toMatrix() {
		Matrix m = new Matrix(4, 1);
		m.values[0][0] = x;
		m.values[1][0] = y;
		m.values[2][0] = z;
		m.values[3][0] = i;
		return m;
	}

	public Vec4f div(float divider) {
		return new Vec4f(x / divider, y / divider, z / divider, i / divider);
	}

	public Vec2f toVec2f() {
		return new Vec2f(x, y);
	}

	public Vec3f toVec3f() {
		return new Vec3f(x, y, z);
	}

	public float norm() {
		return (float) Math.sqrt(x * x + y * y + z * z + i * i);
	}

	public Vec4f normalize() {
		float norm = norm();
		x = x / norm;
		y = y / norm;
		z = z / norm;
		i = i / norm;
		return this;
	}

	public static Vec4f parse(String line) {
		return Utils.parseVec4fFromString(line, " ");
	}

	public float[] toArray() {
		return new float[] { x, y, z, i };
	}
}
