package e3dgl.geometry;

public class Matrix {

	private static Vec4f v4fs = new Vec4f();

	private static Vec3f v3fs = new Vec3f();
	
	private static Matrix m3ls = new Matrix(3,1);

	public float values[][];

	public Matrix(int rows, int cols) {
		values = new float[rows][cols];
	}

	public Vec3i m4toVector3I() {
		return new Vec3i(values[0][0] / values[3][0], values[1][0]
				/ values[3][0], values[2][0] / values[3][0]);
	}

	public Vec2f vertToVector2F() {
		return new Vec2f(values[0][0], values[1][0]);
	}

	public static Matrix identity(int size) {
		Matrix matrix = new Matrix(size, size);
		for (int i = 0; i < size; i++) {
			matrix.values[i][i] = 1;
		}
		return matrix;
	}

	public Matrix product(Matrix matrix) {
		Matrix C = new Matrix(values.length, matrix.values[0].length);
		for (int k = 0; k < matrix.values[0].length; k++) {
			for (int i = 0; i < values.length; i++) {
				float temp = 0;
				for (int j = 0; j < matrix.values.length; j++) {
					temp += values[i][j] * matrix.values[j][k];
				}
				C.values[i][k] = temp;
			}
		}
		return C;
	}

	public Matrix product(Vec4f vec) {
		return product(vec.toMatrixSingleton());
	}

	public Matrix product4(Vec3f vec) {
		return product(vec.toMatrix4Singleton());
	}

	public Matrix product(Vec3f vec) {
		return product(vec.toMatrixSingleton());
	}
	
	public Matrix product(float[] vec) {
		m3ls.values[0][0] = vec[0];
		m3ls.values[1][0] = vec[1];
		m3ls.values[2][0] = vec[2];
		return product(m3ls);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < values.length; i++) {
			for (int j = 0; j < values[i].length; j++) {
				if (j > 0)
					sb.append(" ");
				sb.append(values[i][j]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	public void setColumn(int j, Vec2f vector) {
		values[0][j] = vector.x;
		values[1][j] = vector.y;
	}

	public Matrix transpoze() {
		Matrix matrix = new Matrix(values[0].length, values.length);
		for (int i = 0; i < values.length; i++) {
			for (int j = 0; j < values[0].length; j++) {
				matrix.values[j][i] = values[i][j];
			}
		}
		return matrix;
	}

	public Matrix div(float value) {
		for (int i = 0; i < values.length; i++) {
			for (int j = 0; j < values[0].length; j++) {
				values[i][j] = values[i][j] / value;
			}
		}
		return this;
	}

	public Matrix invert() {
		return adjugate().transpoze().div(det());
	}

	public Matrix invert_transpoze() {
		return invert().transpoze();
	}

	public Matrix getMinor(int row, int col) {
		Matrix matrix = new Matrix(values.length - 1, values[0].length - 1);
		for (int i = 0; i < matrix.values.length; i++) {
			for (int j = 0; j < matrix.values[0].length; j++) {
				matrix.values[i][j] = values[i < row ? i : i + 1][j < col ? j
						: j + 1];
			}
		}
		return matrix;
	}

	public float cofactor(int row, int col) {
		return getMinor(row, col).det() * ((row + col) % 2 == 0 ? 1 : -1);
	}

	public Matrix adjugate() {
		Matrix matrix = new Matrix(values.length, values[0].length);
		for (int i = 0; i < matrix.values.length; i++) {
			for (int j = 0; j < matrix.values[0].length; j++) {
				matrix.values[i][j] = cofactor(i, j);
			}
		}
		return matrix;
	}

	public float det() {
		if (values.length == 0)
			return 0;
		if (values.length == 1)
			return values[0][0];
		if (values.length == 2)
			return values[0][0] * values[1][1] - values[0][1] * values[1][0];
		float det = 0;
		for (int i = 0; i < values[0].length; i++) {
			det += (i % 2 == 0 ? 1 : -1) * values[0][i] * getMinor(0, i).det();
		}
		return det;
	}

	public Vec4f toVector4f() {
		return new Vec4f(values[0][0], values[1][0], values[2][0], values[3][0]);
	}

	public Vec4f toVector4fSingleton() {
		v4fs.x = values[0][0];
		v4fs.y = values[1][0];
		v4fs.z = values[2][0];
		v4fs.i = values[3][0];
		return v4fs;
	}

	public Vec3f toVector3fSingleton() {
		v3fs.x = values[0][0];
		v3fs.y = values[1][0];
		v3fs.z = values[2][0];
		return v3fs;
	}

	public Vec3f toVector3f() {
		return new Vec3f(values[0][0], values[1][0], values[2][0]);
	}

	public void setColumn(int j, Vec4f gl_Vertex) {
		values[0][j] = gl_Vertex.x;
		values[1][j] = gl_Vertex.y;
		values[2][j] = gl_Vertex.z;
		values[3][j] = gl_Vertex.i;
	}

	public Matrix productAndTranspoze(Matrix matrix) {
		Matrix C = new Matrix(matrix.values[0].length, values.length);
		for (int k = 0; k < matrix.values[0].length; k++) {
			for (int i = 0; i < values.length; i++) {
				float temp = 0;
				for (int j = 0; j < matrix.values.length; j++) {
					temp += values[i][j] * matrix.values[j][k];
				}
				C.values[k][i] = temp;
			}
		}
		return C;
	}

}
