package e3dgl.geometry;

public class Vec2i {

	public int x;

	public int y;

	public Vec2i(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Vec2f toVec2f() {
		return new Vec2f(x, y);
	}

}
