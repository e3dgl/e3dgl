package e3dgl.engine;

public class ContextProvider implements IContextProvider {

	private Context context;

	@Override
	public Context getContext() {
		return context;
	}

	@Override
	public void setContext(Context context) {
		this.context = context;
	}

}
