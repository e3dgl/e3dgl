package e3dgl.engine;

public interface IContextProvider {

	public Context getContext();

	public void setContext(Context context);

}
