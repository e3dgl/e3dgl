package e3dgl.engine.material.base;

import e3dgl.engine.material.Material;

public class DefaultMaterial extends Material {

	public static final String MATERIAL_DEFAULT = "Default material";

	public DefaultMaterial() {
		super(MATERIAL_DEFAULT);
	}

}
