package e3dgl.engine.material;

import e3dgl.engine.model.Color4f;

public class Material implements IMaterial {

	private String name;

	private Color4f ambient = new Color4f();

	private Color4f specular = new Color4f();

	private Color4f diffuse = new Color4f();

	public Material(String name) {
		this.name = name;
	}

	@Override
	public Color4f getAmbient() {
		return ambient;
	}

	@Override
	public void setAmbient(Color4f color) {
		setAmbient(color.r, color.g, color.b, color.a);
	}

	@Override
	public Color4f getSpecular() {
		return specular;
	}

	@Override
	public void setSpecular(Color4f color) {
		setSpecular(color.r, color.g, color.b, color.a);
	}

	@Override
	public Color4f getDiffuse() {
		return diffuse;
	}

	@Override
	public void setDiffuse(Color4f color) {
		setDiffuse(color.r, color.g, color.b, color.a);
	}

	@Override
	public String getName() {
		return name;
	}

	public void setDiffuse(float r, float g, float b, float a) {
		diffuse.r = r;
		diffuse.g = g;
		diffuse.b = b;
		diffuse.a = a;
	}

	public void setAmbient(float r, float g, float b, float a) {
		ambient.r = r;
		ambient.g = g;
		ambient.b = b;
		ambient.a = a;
	}

	public void setSpecular(float r, float g, float b, float a) {
		specular.r = r;
		specular.g = g;
		specular.b = b;
		specular.a = a;
	}

}
