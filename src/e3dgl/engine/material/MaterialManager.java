package e3dgl.engine.material;

import java.util.HashMap;
import java.util.Map;

import e3dgl.engine.Context;
import e3dgl.engine.ContextProvider;
import e3dgl.engine.material.base.DefaultMaterial;

public class MaterialManager extends ContextProvider {

	public static final String MATERIAL_ROLE_DEFAULT = "Default material role";

	private Map<String, IMaterial> materials = new HashMap<String, IMaterial>();

	private Map<String, IMaterial> materialsByRole = new HashMap<String, IMaterial>();

	public MaterialManager(Context context) {
		setContext(context);
		addMaterialByRole(MATERIAL_ROLE_DEFAULT, new DefaultMaterial());
	}

	public void addMaterial(IMaterial material) {
		materials.put(material.getName(), material);
	}

	public void addMaterialByRole(String role, IMaterial material) {
		materialsByRole.put(role, material);
	}

	public IMaterial getDefaultMaterial() {
		return materialsByRole.get(MATERIAL_ROLE_DEFAULT);
	}

	public IMaterial getMaterial(String materialName) {
		return materials.get(materialName);
	}

	public Map<String, IMaterial> getMaterials() {
		return materials;
	}

}
