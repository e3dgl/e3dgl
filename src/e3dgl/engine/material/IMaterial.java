package e3dgl.engine.material;

import e3dgl.engine.model.Color4f;

public interface IMaterial {

	public String getName();

	public Color4f getAmbient();

	public Color4f getDiffuse();

	public Color4f getSpecular();

	public void setAmbient(Color4f color);

	public void setDiffuse(Color4f color);

	public void setSpecular(Color4f color);

}
