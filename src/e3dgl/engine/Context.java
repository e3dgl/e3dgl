package e3dgl.engine;

import e3dgl.app.Config;
import e3dgl.engine.camera.CameraManager;
import e3dgl.engine.console.Stats;
import e3dgl.engine.material.MaterialManager;
import e3dgl.engine.model.IModel;
import e3dgl.engine.model.Scene;
import e3dgl.engine.shader.ShaderProgramManager;
import e3dgl.engine.texture.TextureManager;

public class Context {

	private int unqiueIdentifierCounter = 0;

	private CameraManager cameraManager;

	private TextureManager textureManager;

	private ShaderProgramManager shaderManager;

	private MaterialManager materialManager;

	private IModel paintedModel;

	private Stats stats;

	private Config config;
	
	private Scene scene;

	public Context(Config config) {
		this.config = config;
	}

	public Stats getStats() {
		if (stats == null) {
			stats = new Stats(this);
		}
		return stats;
	}

	public int getWidth() {
		return config.getWidth();
	}

	public int getHeight() {
		return config.getHeight();
	}

	public MaterialManager getMaterialManager() {
		if (materialManager == null) {
			materialManager = new MaterialManager(this);
		}
		return materialManager;
	}

	public CameraManager getCameraManager() {
		if (cameraManager == null) {
			cameraManager = new CameraManager(this);
		}
		return cameraManager;
	}

	public TextureManager getTextureManager() {
		if (textureManager == null) {
			textureManager = new TextureManager(this);
		}
		return textureManager;
	}

	public ShaderProgramManager getShaderManager() {
		if (shaderManager == null) {
			shaderManager = new ShaderProgramManager(this);
		}
		return shaderManager;
	}

	public void beginPaintModel(IModel model) {
		this.paintedModel = model;
		getStats().paintModel(paintedModel);
	}

	public void endPaintModel() {
		getStats().endPaintModel(paintedModel);
		paintedModel = null;
	}

	public int generateUniqueIdentifier() {
		return unqiueIdentifierCounter++;
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}
	
	public Scene getScene() {
		return scene;
	}

}
