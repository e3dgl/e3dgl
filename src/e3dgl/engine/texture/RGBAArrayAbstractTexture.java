package e3dgl.engine.texture;

import org.lwjgl.opengl.GL11;

import e3dgl.common.Utils;
import e3dgl.engine.Context;

public class RGBAArrayAbstractTexture extends AbstractTexture {

	private int[][][] textureArray;

	public RGBAArrayAbstractTexture(Context context, int id) {
		super(context, id);
	}

	public RGBAArrayAbstractTexture(Context context) {
		this(context, context.generateUniqueIdentifier());
	}

	@Override
	public int getWidth() {
		return textureArray.length;
	}

	@Override
	public int getHeight() {
		return textureArray[0].length;
	}

	@Override
	public void load() {
		super.load();
		setOpenGLTextureId(Utils.generateTexture2DStretched(getTextureArray(),
				true));
	}

	@Override
	public void unload() {
		super.unload();
		GL11.glDeleteTextures(getOpenGLTextureId());
	}

	protected int[][][] getTextureArray() {
		return textureArray;
	}

	protected void setTextureArray(int[][][] textureArray) {
		this.textureArray = textureArray;
	}

}
