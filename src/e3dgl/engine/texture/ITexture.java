package e3dgl.engine.texture;

public interface ITexture {

	public int getWidth();

	public int getHeight();

	public int getId();

	public void load();

	public void unload();

	public void reload();

	public boolean isLoaded();

	public void bind();
	
	public void setBindParameter(int id);
	
	public int getBindParameter();

}



