package e3dgl.engine.texture;

import e3dgl.engine.Context;

public class RGBAArrayStaticTexture extends RGBAArrayAbstractTexture {

	public RGBAArrayStaticTexture(Context context, int id,
			int[][][] textureArray) {
		super(context, id);
		setTextureArray(textureArray);
	}

	public RGBAArrayStaticTexture(Context context, int[][][] textureArray) {
		super(context);
		setTextureArray(textureArray);
	}

}
