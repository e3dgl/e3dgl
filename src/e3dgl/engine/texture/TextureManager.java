package e3dgl.engine.texture;

import java.util.HashMap;
import java.util.Map;

import e3dgl.engine.Context;
import e3dgl.engine.ContextProvider;

public class TextureManager extends ContextProvider {

	private static int unqiueIdentifierCounter = 0;

	private Map<Integer, ITexture> textures = new HashMap<Integer, ITexture>();

	public TextureManager(Context context) {
		setContext(context);
	}

	public ITexture loadTexture(String string) {
		return addTexture(ImagedTexture.loadFromFile(getContext(), string));
	}

	public ITexture addTexture(ITexture texture) {
		textures.put(texture.getId(), texture);
		return texture;
	}

	public static int generateUniqueIdentifier() {
		return unqiueIdentifierCounter++;
	}

	public ITexture getTexture(int textureId) {
		return textures.get(textureId);
	}

	public Map<Integer, ITexture> getTextures() {
		return textures;
	}

}
