package e3dgl.engine.texture;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.lwjgl.opengl.GL11;

import e3dgl.common.Utils;
import e3dgl.engine.Context;

public class ImagedTexture extends AbstractTexture {

	private BufferedImage image;

	private String file;

	public ImagedTexture(Context context) {
		this(context, context.generateUniqueIdentifier());
	}

	public ImagedTexture(Context context, int id) {
		super(context, id);
	}

	public int getWidth() {
		return image.getWidth();
	}

	public int getHeight() {
		return image.getHeight();
	}

	public void setFile(String file) {
		this.file = file;
		load();
	}

	public String getFile() {
		return file;
	}

	private static void fillTextureData(ImagedTexture texture, String path) {
		if (path.endsWith(".tga")) {
			BufferedImage image = Utils.readAndFlipTGAImage(path);
			texture.image = image;
			texture.file = path;
		} else {
			BufferedImage image;
			try {
				image = Utils.readAndFlipImage(path);
				texture.image = image;
				texture.file = path;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static ITexture loadFromFile(Context context, String path) {
		ImagedTexture texture = new ImagedTexture(context);
		fillTextureData(texture, path);
		return texture;
	}

	@Override
	public void load() {
		super.load();
		fillTextureData(this, file);
		setOpenGLTextureId(Utils.loadTexture2DStretchedSmoothed(image));
	}

	@Override
	public void unload() {
		super.unload();
		GL11.glDeleteTextures(getOpenGLTextureId());
	}

}
