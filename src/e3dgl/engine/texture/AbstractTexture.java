package e3dgl.engine.texture;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import e3dgl.engine.Context;
import e3dgl.engine.ContextProvider;

public abstract class AbstractTexture extends ContextProvider implements
		ITexture {

	private int openGLTextureId = -1;

	private int id = -1;

	private int textureID = -1;

	private boolean loaded = false;

	public AbstractTexture(Context context, int id) {
		setContext(context);
		this.id = id;
	}

	public AbstractTexture(Context context) {
		this(context, context.generateUniqueIdentifier());
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void reload() {
		unload();
		load();
	}

	@Override
	public void load() {
		GL13.glActiveTexture(getBindParameter());
		//GL11.glEnable(GL11.GL_TEXTURE_2D);
		loaded = true;
	}

	@Override
	public void unload() {
		loaded = false;
	}

	@Override
	public boolean isLoaded() {
		return loaded;
	}

	protected int getOpenGLTextureId() {
		return openGLTextureId;
	}

	protected void setOpenGLTextureId(int id) {
		this.openGLTextureId = id;
	}

	public void setBindParameter(int textureId) {
		this.textureID = textureId;
	}

	public int getBindParameter() {
		return this.textureID;
	}

	@Override
	public void bind() {
		if (!isLoaded())
			load();
		GL13.glActiveTexture(getBindParameter());
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, openGLTextureId);
	}

}
