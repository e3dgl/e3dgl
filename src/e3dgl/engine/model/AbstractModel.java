package e3dgl.engine.model;

import java.util.HashSet;
import java.util.Set;

import org.lwjgl.opengl.GL11;

import e3dgl.common.Utils;
import e3dgl.engine.Context;
import e3dgl.engine.material.IMaterial;
import e3dgl.engine.shader.IShaderProgram;
import e3dgl.engine.texture.ITexture;

public abstract class AbstractModel implements IModel {

	private Set<IModel> childs = new HashSet<IModel>();

	private Color4f color = new Color4f(0, 0, 0, 1);

	private int texDiffuseId = -1;
	
	private int texSpecularId = -1;
	
	private int texNormalId = -1;
	
	private String shaderName;
	
	private String materialName;

	@Override
	public void paint(Context context) {
		context.beginPaintModel(this);
		ITexture diffuseTex = getDiffuseTexture(context);
		ITexture specularTex = getSpecularTexture(context);
		ITexture normalTex = getNormalTexture(context);
		if (diffuseTex == null && specularTex == null && normalTex == null) {
			GL11.glColor4f(color.r, color.g, color.b, color.a);
		} else {
			//GL11.glColor4f(color.r, color.g, color.b, color.a);
			GL11.glColor4f(Utils.fullRGBAColor[0], Utils.fullRGBAColor[1], Utils.fullRGBAColor[2], Utils.fullRGBAColor[3]);
			if(diffuseTex!=null)
				diffuseTex.bind();
			if(normalTex!=null)
				normalTex.bind();
			if(specularTex!=null)
				specularTex.bind();
		}
		IShaderProgram shaderProgram = context.getShaderManager().getShaderProgram(getShaderName());
		if(shaderProgram != null) { 
			shaderProgram.setDiffuseMap(diffuseTex);
			shaderProgram.setSpecMap(specularTex);
			shaderProgram.setNormalMap(normalTex);
			shaderProgram.setLights(context.getScene().getLights());
			shaderProgram.setMaterial(getMaterial(context));
			shaderProgram.setColor(getColor());
			shaderProgram.use();
		}
		paintCurrent(context);
		if(shaderProgram != null)
			shaderProgram.unuse();
		context.endPaintModel();
		paintChildrens(context);
	}
	
	public String getShaderName() {
		return shaderName;
	}

	public void setShaderName(String shaderName) {
		this.shaderName = shaderName;
	}
	
	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public IMaterial getMaterial(Context context) {
		if(getMaterialName() == null)
			return context.getMaterialManager().getDefaultMaterial();	
		return context.getMaterialManager().getMaterial(getMaterialName());
	}	
	
	public IShaderProgram getShader(Context context) {
		if(getShaderName() == null)
			return context.getShaderManager().getDefaultShaderProgram();	
		return context.getShaderManager().getShaderProgram(getShaderName());
	}

	public void setDiffuseTexture(ITexture texture) {
		setTexDiffuseId(texture.getId());
	}
	
	public void setSpecularTexture(ITexture texture) {
		setTexSpecularId(texture.getId());
	}

	public void setNormalTexture(ITexture texture) {
		setTexNormalId(texture.getId());
	}
	
	protected ITexture getDiffuseTexture(Context context) {
		return context.getTextureManager().getTexture(getTexDiffuseId());
	}
	
	protected ITexture getSpecularTexture(Context context) {
		return context.getTextureManager().getTexture(getTexSpecularId());
	}

	protected ITexture getNormalTexture(Context context) {
		return context.getTextureManager().getTexture(getTexNormalId());
	}
	
	public void setTexDiffuseId(int texDiffuseId) {
		this.texDiffuseId = texDiffuseId;
	}

	public void setTexSpecularId(int texSpecularId) {
		this.texSpecularId = texSpecularId;
	}

	public void setTexNormalId(int texNormalId) {
		this.texNormalId = texNormalId;
	}

	public int getTexDiffuseId() {
		return texDiffuseId;
	}

	public int getTexSpecularId() {
		return texSpecularId;
	}

	public int getTexNormalId() {
		return texNormalId;
	}

	protected void paintCurrent(Context context) {
	}

	private void paintChildrens(Context context) {
		for (IModel model : childs) {
			model.paint(context);
		}
	}

	public void addModel(IModel model) {
		childs.add(model);
	}

	@Override
	public void tick(Context context) {
		tickChildrens(context);
		tickCurrent(context);
	}

	protected void tickCurrent(Context context) {

	}

	private void tickChildrens(Context context) {
		for (IModel model : childs) {
			model.tick(context);
		}
	}

	public Set<IModel> getChild() {
		return childs;
	}

	public Color4f getColor() {
		return color;
	}
	
	@Override
	public void setColor(Color4f color) {
		this.color.r = color.r;
		this.color.g = color.g;
		this.color.b = color.b;
		this.color.a = color.a;
	}

	@Override
	public IModel copy() {
		AbstractModel model = createInstance();
		model.setColor(color);
		model.setTexDiffuseId(getTexDiffuseId());
		model.setTexSpecularId(getTexSpecularId());
		model.setTexNormalId(getTexNormalId());
		model.setShaderName(getShaderName());
		model.setMaterialName(getMaterialName());
		for (IModel child : childs) {
			model.addModel(child.copy());
		}
		return model;
	}

	protected abstract AbstractModel createInstance();

}
