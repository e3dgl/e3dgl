package e3dgl.engine.model;

import e3dgl.engine.Context;

public interface ISceneInitializer {

	public void init(Context context, Scene scene);

}
