package e3dgl.engine.model;

import java.util.HashSet;
import java.util.Set;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import e3dgl.common.Utils;
import e3dgl.engine.Context;
import e3dgl.engine.shader.IShaderProgram;
import e3dgl.geometry.Matrix;
import e3dgl.geometry.Vec3f;

public class PolygonalModel extends AbstractModel {
	
	public static final int INDEX_VERTEX = 0;
	
	public static final int INDEX_UV = INDEX_VERTEX + 1;
	
	public static final int INDEX_NORMALS = INDEX_UV+ 1;
	
	public static final int INDEX_TANGENT = INDEX_NORMALS + 1;
	
	public static final int INDEX_BITANGENT = INDEX_TANGENT + 1;
	
	public static final int INDEX_X = 0;
	
	public static final int INDEX_Y = INDEX_X + 1;
	
	public static final int INDEX_Z = INDEX_Y + 1;
	
	public static final int INDEX_VERTEX_0 = 0;
	
	public static final int INDEX_VERTEX_1 = INDEX_VERTEX_0 + 1;
	
	public static final int INDEX_VERTEX_2 = INDEX_VERTEX_1 + 1;
	
	private float[][] vertexes; 
	
	private float[][] uv;
	
	private float[][] normals;
	
	private float[][] tangents;
	
	private float[][] bitangents;
	
	private int[][][] polygons;
	
	private Vec3f baseMovement = new Vec3f();
	
	private Vec3f baseRotation = new Vec3f();
	
	private String obj;
	
	public PolygonalModel() {
	}
	
	public void setVertexes(float[][] vertexes){
		this.vertexes = vertexes;
	}
	
	public void setPolygons(int[][][] polygons){
		this.polygons = polygons;
	}
	
	public void setUVs(float[][] uv){
		this.uv = uv;
	}
	
	public void setNormals(float[][] normalsArray) {
		this.normals = normalsArray;
	}
	
	@Override
	public void paintCurrent(Context context) {
		IShaderProgram shaderProgram = getShader(context);
		GL11.glBegin(GL11.GL_TRIANGLES);
		for(int[][] polygon : polygons) {
			GL20.glVertexAttrib3f(shaderProgram.getTangentIndex(), tangents[polygon[INDEX_VERTEX][INDEX_VERTEX_0]][INDEX_X], tangents[polygon[INDEX_VERTEX][INDEX_VERTEX_0]][INDEX_X], tangents[polygon[INDEX_VERTEX][INDEX_VERTEX_0]][INDEX_X]);
			GL20.glVertexAttrib3f(shaderProgram.getBiTangentIndex(), bitangents[polygon[INDEX_VERTEX][INDEX_VERTEX_0]][INDEX_X], bitangents[polygon[INDEX_VERTEX][INDEX_VERTEX_0]][INDEX_X], bitangents[polygon[INDEX_VERTEX][INDEX_VERTEX_0]][INDEX_X]);
			GL11.glTexCoord3f(      uv[polygon[INDEX_UV    ][INDEX_VERTEX_0]][INDEX_X],       uv[polygon[INDEX_UV    ][INDEX_VERTEX_0]][INDEX_Y],       uv[polygon[INDEX_UV    ][INDEX_VERTEX_0]][INDEX_Z]);
			GL11.glVertex3f(  vertexes[polygon[INDEX_VERTEX][INDEX_VERTEX_0]][INDEX_X], vertexes[polygon[INDEX_VERTEX][INDEX_VERTEX_0]][INDEX_Y], vertexes[polygon[INDEX_VERTEX][INDEX_VERTEX_0]][INDEX_Z]);

			GL20.glVertexAttrib3f(shaderProgram.getTangentIndex(), tangents[polygon[INDEX_VERTEX][INDEX_VERTEX_1]][INDEX_X], tangents[polygon[INDEX_VERTEX][INDEX_VERTEX_1]][INDEX_X], tangents[polygon[INDEX_VERTEX][INDEX_VERTEX_1]][INDEX_X]);
			GL20.glVertexAttrib3f(shaderProgram.getBiTangentIndex(), bitangents[polygon[INDEX_VERTEX][INDEX_VERTEX_1]][INDEX_X], bitangents[polygon[INDEX_VERTEX][INDEX_VERTEX_1]][INDEX_X], bitangents[polygon[INDEX_VERTEX][INDEX_VERTEX_1]][INDEX_X]);
			GL11.glTexCoord3f(      uv[polygon[INDEX_UV    ][INDEX_VERTEX_1]][INDEX_X],       uv[polygon[INDEX_UV    ][INDEX_VERTEX_1]][INDEX_Y],       uv[polygon[INDEX_UV    ][INDEX_VERTEX_1]][INDEX_Z]);
			GL11.glVertex3f(  vertexes[polygon[INDEX_VERTEX][INDEX_VERTEX_1]][INDEX_X], vertexes[polygon[INDEX_VERTEX][INDEX_VERTEX_1]][INDEX_Y], vertexes[polygon[INDEX_VERTEX][INDEX_VERTEX_1]][INDEX_Z]);
			
			GL20.glVertexAttrib3f(shaderProgram.getTangentIndex(), tangents[polygon[INDEX_VERTEX][INDEX_VERTEX_1]][INDEX_X], tangents[polygon[INDEX_VERTEX][INDEX_VERTEX_1]][INDEX_X], tangents[polygon[INDEX_VERTEX][INDEX_VERTEX_2]][INDEX_X]);
			GL20.glVertexAttrib3f(shaderProgram.getBiTangentIndex(), bitangents[polygon[INDEX_VERTEX][INDEX_VERTEX_1]][INDEX_X], bitangents[polygon[INDEX_VERTEX][INDEX_VERTEX_1]][INDEX_X], bitangents[polygon[INDEX_VERTEX][INDEX_VERTEX_2]][INDEX_X]);
			GL11.glTexCoord3f(      uv[polygon[INDEX_UV    ][INDEX_VERTEX_2]][INDEX_X],       uv[polygon[INDEX_UV    ][INDEX_VERTEX_2]][INDEX_Y],       uv[polygon[INDEX_UV    ][INDEX_VERTEX_2]][INDEX_Z]);
			GL11.glVertex3f(  vertexes[polygon[INDEX_VERTEX][INDEX_VERTEX_2]][INDEX_X], vertexes[polygon[INDEX_VERTEX][INDEX_VERTEX_2]][INDEX_Y], vertexes[polygon[INDEX_VERTEX][INDEX_VERTEX_2]][INDEX_Z]);
		}
		GL11.glEnd();
	}
	
	@Override
	public PolygonalModel copy() {
		PolygonalModel model = (PolygonalModel)super.copy();
		model.vertexes = new float[vertexes.length][]; 
		for(int i=0; i<vertexes.length; i++) {
			model.vertexes[i] = new float[vertexes[i].length];
			for(int j=0; j<vertexes[i].length; j++) {
				model.vertexes[i][j] = vertexes[i][j];
			}
		}
		model.normals = new float[normals.length][]; 
		for(int i=0; i<normals.length; i++) {
			model.normals[i] = new float[normals[i].length];
			for(int j=0; j<normals[i].length; j++) {
				model.normals[i][j] = normals[i][j];
			}
		}
		model.uv = new float[uv.length][]; 
		for(int i=0; i<uv.length; i++) {
			model.uv[i] = new float[uv[i].length];
			for(int j=0; j<uv[i].length; j++) {
				model.uv[i][j] = uv[i][j];
			}
		}
		model.tangents = new float[tangents.length][]; 
		for(int i=0; i<tangents.length; i++) {
			model.tangents[i] = new float[tangents[i].length];
			for(int j=0; j<tangents[i].length; j++) {
				model.tangents[i][j] = tangents[i][j];
			}
		}
		model.bitangents = new float[bitangents.length][]; 
		for(int i=0; i<bitangents.length; i++) {
			model.bitangents[i] = new float[bitangents[i].length];
			for(int j=0; j<bitangents[i].length; j++) {
				model.bitangents[i][j] = bitangents[i][j];
			}
		}
		model.polygons = new int[polygons.length][][]; 
		for(int i=0; i<polygons.length; i++) {
			model.polygons[i] = new int[polygons[i].length][];
			for(int j=0; j<polygons[i].length; j++) {
				model.polygons[i][j] = new int[polygons[i][j].length];
				for(int k=0; k<polygons[i][j].length; k++) {
					model.polygons[i][j][k] = polygons[i][j][k]; 
				}
			}
		}


		model.setBaseMovement(baseMovement);
		model.setBaseRotation(baseRotation);
		model.setObjFile(obj);
		return model;
	}
	
	public PolygonalModel rotate(Vec3f rotation) {
		setBaseRotation(rotation);
		rotate(rotation.x, rotation.y, rotation.z);
		return this;
	}
	
	public PolygonalModel move(Vec3f movement) {
		setBaseMovement(movement);
		move(movement.x, movement.y, movement.z);
		return this;
	}
	
	public PolygonalModel rotate(float alpha, float beta, float gamma) {
		Matrix rotX = Utils.getRotXMatrix(alpha);
		Matrix rotY = Utils.getRotYMatrix(beta);
		Matrix rotZ = Utils.getRotZMatrix(gamma);
		for (int i = 0; i < vertexes.length; i++) {
			Vec3f vertex = rotZ.product(rotY.product(rotX.product(vertexes[i]))).toVector3fSingleton();
			vertexes[i][INDEX_X] = vertex.x;
			vertexes[i][INDEX_Y] = vertex.y;
			vertexes[i][INDEX_Z] = vertex.z;
		}
		/*
		for (int i = 0; i < tangents.length; i++) {
			Vec3f tangent = rotZ.product(rotY.product(rotX.product(tangents[i]))).toVector3fSingleton();
			tangents[i][INDEX_X] = tangent.x;
			tangents[i][INDEX_Y] = tangent.y;
			tangents[i][INDEX_Z] = tangent.z;
		}
		for (int i = 0; i < bitangents.length; i++) {
			Vec3f bitangent = rotZ.product(rotY.product(rotX.product(bitangents[i]))).toVector3fSingleton();
			bitangents[i][INDEX_X] = bitangent.x;
			bitangents[i][INDEX_Y] = bitangent.y;
			bitangents[i][INDEX_Z] = bitangent.z;
		}
		*/
		return this;
	}
	
	public PolygonalModel move(float deltaX, float deltaY, float deltaZ) {
		for (int i = 0; i < vertexes.length; i++) {
			vertexes[i][INDEX_X] += deltaX;
			vertexes[i][INDEX_Y] += deltaY;
			vertexes[i][INDEX_Z] += deltaZ;
		}
		return this;
	}

	@Override
	protected PolygonalModel createInstance() {
		return new PolygonalModel();
	}

	public Vec3f getBaseMovement() {
		return baseMovement;
	}

	public void setBaseMovement(float deltaX, float deltaY, float deltaZ) {
		this.baseMovement.x = deltaX;
		this.baseMovement.y = deltaY;
		this.baseMovement.z = deltaZ;
	}
	
	public void setBaseMovement(Vec3f baseMovement) {
		setBaseMovement(baseMovement.x, baseMovement.y, baseMovement.z);
	}

	public Vec3f getBaseRotation() {
		return baseRotation;
	}

	public void setBaseRotation(float alpha, float beta, float gamma) {
		this.baseRotation.x = alpha;
		this.baseRotation.y = beta;
		this.baseRotation.z = gamma;
	}
	
	public void setBaseRotation(Vec3f baseRotation) {
		setBaseRotation(baseRotation.x, baseRotation.y, baseRotation.z);
	}

	public String getObjFile() {
		return obj;
	}

	public void setObjFile(String obj) {
		this.obj = obj;
	}

	public void fillFromObjFile(String path) {
		Utils.fillFromObjFile(this, path);
	}

	public void calculateTBNValues() {
		float [][][] tt = new float[polygons.length][3][3];
		float [][][] tbt = new float[polygons.length][3][3];
		
		for(int i=0; i<polygons.length; i++) {
			int[][] polygon = polygons[i];
			
			Vec3f v0 = new Vec3f(vertexes[polygon[INDEX_VERTEX][0]][INDEX_X], vertexes[polygon[INDEX_VERTEX][0]][INDEX_Y], vertexes[polygon[INDEX_VERTEX][0]][INDEX_Z]);
			Vec3f v1 = new Vec3f(vertexes[polygon[INDEX_VERTEX][1]][INDEX_X], vertexes[polygon[INDEX_VERTEX][1]][INDEX_Y], vertexes[polygon[INDEX_VERTEX][1]][INDEX_Z]);
			Vec3f v2 = new Vec3f(vertexes[polygon[INDEX_VERTEX][2]][INDEX_X], vertexes[polygon[INDEX_VERTEX][2]][INDEX_Y], vertexes[polygon[INDEX_VERTEX][2]][INDEX_Z]);
	
			Vec3f uv0 = new Vec3f(     uv[polygon[INDEX_UV    ][0]][INDEX_X],       uv[polygon[INDEX_UV    ][0]][INDEX_Y],       uv[polygon[INDEX_UV    ][0]][INDEX_Z]);
			Vec3f uv1 = new Vec3f(     uv[polygon[INDEX_UV    ][1]][INDEX_X],       uv[polygon[INDEX_UV    ][1]][INDEX_Y],       uv[polygon[INDEX_UV    ][1]][INDEX_Z]);
			Vec3f uv2 = new Vec3f(     uv[polygon[INDEX_UV    ][2]][INDEX_X],       uv[polygon[INDEX_UV    ][2]][INDEX_Y],       uv[polygon[INDEX_UV    ][2]][INDEX_Z]);
	
			Vec3f deltaPos1 = v1.minus(v0);
			Vec3f deltaPos2 = v2.minus(v0);
			
			Vec3f deltaUV1 = uv1.minus(uv0);
			Vec3f deltaUV2 = uv2.minus(uv0);
	
			
			float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
			Vec3f tangent = (deltaPos1.mul(deltaUV2.y).minus(deltaPos2.mul(deltaUV1.y))).mul(r);
			Vec3f bitangent = (deltaPos2.mul(deltaUV1.x).minus(deltaPos1.mul( deltaUV2.x))).mul(r);
			
			tt[i][INDEX_VERTEX_0][INDEX_X] = tangent.x;
			tt[i][INDEX_VERTEX_0][INDEX_Y] = tangent.y;
			tt[i][INDEX_VERTEX_0][INDEX_Z] = tangent.z;
			tt[i][INDEX_VERTEX_1][INDEX_X] = tangent.x;
			tt[i][INDEX_VERTEX_1][INDEX_Y] = tangent.y;
			tt[i][INDEX_VERTEX_1][INDEX_Z] = tangent.z;
			tt[i][INDEX_VERTEX_2][INDEX_X] = tangent.x;
			tt[i][INDEX_VERTEX_2][INDEX_Y] = tangent.y;
			tt[i][INDEX_VERTEX_2][INDEX_Z] = tangent.z;
			
			tbt[i][INDEX_VERTEX_0][INDEX_X] = bitangent.x;
			tbt[i][INDEX_VERTEX_0][INDEX_Y] = bitangent.y;
			tbt[i][INDEX_VERTEX_0][INDEX_Z] = bitangent.z;
			tbt[i][INDEX_VERTEX_1][INDEX_X] = bitangent.x;
			tbt[i][INDEX_VERTEX_1][INDEX_Y] = bitangent.y;
			tbt[i][INDEX_VERTEX_1][INDEX_Z] = bitangent.z;
			tbt[i][INDEX_VERTEX_2][INDEX_X] = bitangent.x;
			tbt[i][INDEX_VERTEX_2][INDEX_Y] = bitangent.y;
			tbt[i][INDEX_VERTEX_2][INDEX_Z] = bitangent.z;
		}
		
		Set<Integer> vertexIndexes = new HashSet<Integer>();
		for(int i=0; i<polygons.length; i++) {
			vertexIndexes.add(polygons[i][INDEX_VERTEX][INDEX_VERTEX_0]);
			vertexIndexes.add(polygons[i][INDEX_VERTEX][INDEX_VERTEX_1]);
			vertexIndexes.add(polygons[i][INDEX_VERTEX][INDEX_VERTEX_2]);
		}

		this.tangents = new float[vertexes.length][3];
		this.bitangents = new float[vertexes.length][3];
		
		Set<Integer> alreadyAveraged = new HashSet<Integer>();
		for(int i=0; i<polygons.length; i++) {
			averageBTTForVertexIndex(tt, tbt, polygons[i][INDEX_VERTEX][INDEX_VERTEX_0], alreadyAveraged);
			averageBTTForVertexIndex(tt, tbt, polygons[i][INDEX_VERTEX][INDEX_VERTEX_1], alreadyAveraged);
			averageBTTForVertexIndex(tt, tbt, polygons[i][INDEX_VERTEX][INDEX_VERTEX_2], alreadyAveraged);
		}
		for(float[] fvec : tangents) {
			Vec3f temp = new Vec3f(fvec[INDEX_X], fvec[INDEX_Y], fvec[INDEX_Z]);
			temp.normalize();
			fvec[INDEX_X] = temp.x;
			fvec[INDEX_Y] = temp.y;
			fvec[INDEX_Z] = temp.z;
		}
		for(float[] fvec : bitangents) {
			Vec3f temp = new Vec3f(fvec[INDEX_X], fvec[INDEX_Y], fvec[INDEX_Z]);
			temp.normalize();
			fvec[INDEX_X] = temp.x;
			fvec[INDEX_Y] = temp.y;
			fvec[INDEX_Z] = temp.z;
		}
	}
	
	private void averageBTTForVertexIndex(float[][][] tt, float[][][] tbt, int vertexNumber, Set<Integer> alreadyAveraged) {
		if(alreadyAveraged.contains(vertexNumber))
			return;
		averageBTT(tt, tbt, vertexNumber, 0, alreadyAveraged);
	}
	
	private void averageBTT(float tt[][][], float tbt[][][], int vertexNumber, int vertex, Set<Integer> alreadyAveraged) {
		average(tt, this.tangents, vertexNumber, vertex, alreadyAveraged);
		average(tbt, this.bitangents, vertexNumber, vertex, alreadyAveraged);
	}

	private void average(float tt[][][], float out_values[][], int vertexNumber, int vertex, Set<Integer> alreadyAveraged) {
		float[] t = new float[]{0,0,0};
		for(int j=0; j<polygons.length; j++) {
			if(polygons[j][INDEX_VERTEX][INDEX_VERTEX_0] == vertexNumber) {
				t[INDEX_X] += tt[j][INDEX_VERTEX_0][INDEX_X];
				t[INDEX_Y] += tt[j][INDEX_VERTEX_0][INDEX_Y];
				t[INDEX_Z] += tt[j][INDEX_VERTEX_0][INDEX_Z];
			}
			if(polygons[j][INDEX_VERTEX][INDEX_VERTEX_1] == vertexNumber) {
				t[INDEX_X] += tt[j][INDEX_VERTEX_1][INDEX_X];
				t[INDEX_Y] += tt[j][INDEX_VERTEX_1][INDEX_Y];
				t[INDEX_Z] += tt[j][INDEX_VERTEX_1][INDEX_Z];
			}
			if(polygons[j][INDEX_VERTEX][INDEX_VERTEX_2] == vertexNumber) {
				t[INDEX_X] += tt[j][INDEX_VERTEX_2][INDEX_X];
				t[INDEX_Y] += tt[j][INDEX_VERTEX_2][INDEX_Y];
				t[INDEX_Z] += tt[j][INDEX_VERTEX_2][INDEX_Z];
			}
		}
		out_values[vertexNumber][INDEX_X] = t[INDEX_X];
		out_values[vertexNumber][INDEX_Y] = t[INDEX_Y];
		out_values[vertexNumber][INDEX_Z] = t[INDEX_Z];
	}
}


