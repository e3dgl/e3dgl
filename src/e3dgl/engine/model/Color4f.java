package e3dgl.engine.model;

import e3dgl.common.Utils;

public class Color4f {

	public float r;

	public float g;

	public float b;

	public float a;

	public Color4f() {
		this(0, 0, 0, 0);
	}

	public Color4f(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public String toString() {
		return r + " " + g + " " + b + " " + a;
	}

	public Color4f copy() {
		return new Color4f(r, g, b, a);
	}

	public static Color4f parse(String line) {
		return Utils.parseColor4fFromString(line, " ");
	}

	public float[] toArray() {
		return new float[] { r, g, b, a };
	}

}
