package e3dgl.engine.model.light;

import e3dgl.engine.model.Color4f;
import e3dgl.engine.model.IEntity;
import e3dgl.geometry.Vec3f;
import e3dgl.geometry.Vec4f;

public interface ILight extends IEntity {

	public Vec4f getPosition();

	public void setPosition(Vec4f position);

	public Vec3f getSpotDirection();

	public void setSpotDirection(Vec3f position);

	public Color4f getAmbient();

	public void setAmbient(Color4f ambient);

	public Color4f getDiffuse();

	public void setDiffuse(Color4f diffuse);

	public Color4f getSpecular();

	public void setSpecular(Color4f specular);

	public void enable();

	public void disable();

	public int getId();

	public void setId(int id);

	public int getOpenGLId();

	public float getSpotExponent();

	public void setSpotExponent(float spotExponent);

	public float getSpotCutoff();

	public void setSpotCutoff(float spotCutoff);

	public float getConstantAttenuation();

	public void setConstantAttenuation(float constantAttenuation);

	public float getLinearAttenuation();

	public void setLinearAttenuation(float linearAttenuation);

	public float getQuadraticAttenuation();

	public void setQuadraticAttenuation(float quadraticAttenuation);
}
