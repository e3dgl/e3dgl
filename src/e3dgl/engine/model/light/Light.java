package e3dgl.engine.model.light;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import org.lwjgl.opengl.GL11;

import e3dgl.common.Utils;
import e3dgl.engine.model.Color4f;
import e3dgl.geometry.Vec3f;
import e3dgl.geometry.Vec4f;

public class Light implements ILight {

	private Color4f ambient = new Color4f(0, 0, 0, 1);

	private Color4f diffuse = new Color4f(1, 1, 1, 1);

	private Color4f specular = new Color4f(1, 1, 1, 1);

	private Vec4f position = new Vec4f(0, 0, 1, 0);

	private Vec3f spotDirection = new Vec3f(0, 0, -1);

	private float spotExponent = 0;

	private float spotCutoff = 180;

	private float constantAttenuation = 1;

	private float linearAttenuation = 0;

	private float quadraticAttenuation = 0;

	private int id = -1;

	public Light() {
	}

	@Override
	public Vec4f getPosition() {
		return position;
	}

	@Override
	public void setPosition(Vec4f position) {
		setPosition(position.x, position.y, position.z, position.i);
	}

	public void setPosition(float x, float y, float z, float i) {
		position.x = x;
		position.y = y;
		position.z = z;
		position.i = i;
		fillBuffer(GL11.GL_POSITION, position.toArray());
	}

	@Override
	public Vec3f getSpotDirection() {
		return spotDirection;
	}

	@Override
	public void setSpotDirection(Vec3f spotDirection) {
		setSpotDirection(spotDirection.x, spotDirection.y, spotDirection.z);
	}

	public void setSpotDirection(float x, float y, float z) {
		spotDirection.x = x;
		spotDirection.y = y;
		spotDirection.z = z;
		fillBuffer(GL11.GL_SPOT_DIRECTION, spotDirection.toArray4());
	}

	@Override
	public void setAmbient(Color4f color) {
		setAmbient(color.r, color.g, color.b, color.a);
	}

	public void setAmbient(float r, float g, float b, float a) {
		ambient.r = r;
		ambient.g = g;
		ambient.b = b;
		ambient.a = a;
		fillBuffer(GL11.GL_AMBIENT, ambient.toArray());
	}

	@Override
	public Color4f getAmbient() {
		return ambient;
	}

	@Override
	public void setDiffuse(Color4f diffuse) {
		setDiffuse(diffuse.r, diffuse.g, diffuse.b, diffuse.a);
	}

	public void setDiffuse(float r, float g, float b, float a) {
		diffuse.r = r;
		diffuse.g = g;
		diffuse.b = b;
		diffuse.a = a;
		fillBuffer(GL11.GL_DIFFUSE, diffuse.toArray());
	}

	@Override
	public Color4f getDiffuse() {
		return diffuse;
	}

	@Override
	public void setSpecular(Color4f specular) {
		setDiffuse(specular.r, specular.g, specular.b, specular.a);
	}

	public void setSpecular(float r, float g, float b, float a) {
		specular.r = r;
		specular.g = g;
		specular.b = b;
		specular.a = a;
		fillBuffer(GL11.GL_SPECULAR, specular.toArray());
	}

	@Override
	public Color4f getSpecular() {
		return specular;
	}

	private void fillBuffer(int id, float[] value) {
		ByteBuffer buffer = ByteBuffer.allocateDirect(16);
		buffer.order(ByteOrder.nativeOrder());
		GL11.glLight(getOpenGLId(), id, (FloatBuffer) buffer.asFloatBuffer()
				.put(value).flip());
	}

	private void fillBuffer(int id, float value) {
		fillBuffer(id, new float[] { value, 0, 0, 0 });
	}

	@Override
	public void enable() {
		GL11.glEnable(getOpenGLId());
	}

	@Override
	public int getOpenGLId() {
		return Utils.getLightOpenGLId(getId());
	}

	@Override
	public void disable() {
		GL11.glDisable(getOpenGLId());
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public float getSpotExponent() {
		return spotExponent;
	}

	@Override
	public void setSpotExponent(float spotExponent) {
		this.spotExponent = spotExponent;
		fillBuffer(GL11.GL_SPOT_EXPONENT, spotExponent);
	}

	@Override
	public float getSpotCutoff() {
		return spotCutoff;
	}

	@Override
	public void setSpotCutoff(float spotCutoff) {
		this.spotCutoff = spotCutoff;
		fillBuffer(GL11.GL_SPOT_CUTOFF, spotCutoff);
	}

	@Override
	public float getConstantAttenuation() {
		return constantAttenuation;
	}

	@Override
	public void setConstantAttenuation(float constantAttenuation) {
		this.constantAttenuation = constantAttenuation;
		fillBuffer(GL11.GL_CONSTANT_ATTENUATION, constantAttenuation);
	}

	@Override
	public float getLinearAttenuation() {
		return linearAttenuation;
	}

	@Override
	public void setLinearAttenuation(float linearAttenuation) {
		this.linearAttenuation = linearAttenuation;
		fillBuffer(GL11.GL_LINEAR_ATTENUATION, linearAttenuation);
	}

	@Override
	public float getQuadraticAttenuation() {
		return quadraticAttenuation;
	}

	@Override
	public void setQuadraticAttenuation(float quadraticAttenuation) {
		this.quadraticAttenuation = quadraticAttenuation;
		fillBuffer(GL11.GL_QUADRATIC_ATTENUATION, quadraticAttenuation);
	}

}
