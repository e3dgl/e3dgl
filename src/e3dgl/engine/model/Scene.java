package e3dgl.engine.model;

import java.util.ArrayList;
import java.util.List;

import e3dgl.engine.Context;
import e3dgl.engine.model.light.ILight;

public class Scene extends AbstractModel {

	private List<ILight> lights = new ArrayList<ILight>();

	public void init(Context context, ISceneInitializer sceneInitializer) {
		sceneInitializer.init(context, this);
		context.setScene(this);
	}

	@Override
	public void tickCurrent(Context context) {
	}

	public void addLight(ILight light) {
		lights.add(light);
		light.enable();
	}

	public List<ILight> getLights() {
		return lights;
	}

	@Override
	protected void paintCurrent(Context context) {
		/*
		 * Graphics graphics = context.getGraphics(); graphics.setColor(color);
		 * graphics.fillRect(0, 0, context.getWidth(), context.getHeight());
		 */
	}
	
	@Override
	protected Scene createInstance() {
		return new Scene();
	}

}
