package e3dgl.engine.model;

import java.util.Set;

import e3dgl.engine.Context;
import e3dgl.engine.texture.ITexture;

public interface IModel extends IEntity {

	public void paint(Context context);

	public void tick(Context context);

	public void setColor(Color4f color);

	public void setDiffuseTexture(ITexture texture);

	public IModel copy();

	public String getShaderName();

	public void setShaderName(String shaderName);

	public String getMaterialName();

	public void setMaterialName(String materialName);

	public Set<IModel> getChild();

}
