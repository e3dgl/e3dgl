package e3dgl.engine.model;

import e3dgl.engine.Context;
import e3dgl.engine.model.serialize.Serializer;

public class E3DLoadableSceneInitializer implements ISceneInitializer {

	private String name;

	public E3DLoadableSceneInitializer(String name) {
		this.name = name;
	}

	@Override
	public void init(Context context, Scene scene) {
		Serializer.deserializeFromXML(context, scene, name);
	}

}
