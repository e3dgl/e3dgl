package e3dgl.engine.console;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

import e3dgl.app.Config;
import e3dgl.engine.Context;
import e3dgl.engine.camera.Camera;
import e3dgl.engine.model.ISceneInitializer;
import e3dgl.engine.model.Scene;

public class Console {

	private Config config;

	private Context context;

	private Scene scene;

	private ISceneInitializer sceneInitializer;
	
	private boolean isInitial = true;

	private float speedDivider = 50f;
	
	private float keyboardSpeed = 0.05f;
	
	public Console(Config config, ISceneInitializer sceneInitializer) {
		this.config = config;
		this.sceneInitializer = sceneInitializer;
	}

	public Config getConfig() {
		return config;
	}

	public void start() {
		try {
			Display.setDisplayMode(new DisplayMode(config.getWidth(), config
					.getHeight()));
			Display.create();
			Display.setVSyncEnabled(true);
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}

		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glShadeModel(GL11.GL_SMOOTH);
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		GL11.glClearDepth(1.0);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDepthFunc(GL11.GL_LEQUAL);

		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();

		GLU.gluPerspective(45.0f,
				(float) config.getWidth() / (float) config.getHeight(), 0.1f,
				100.0f);

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);

		init();
		while (!Display.isCloseRequested()) {
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			loop(context);
			pollInput();
			Display.update();
			Display.sync(100);
		}
		Display.destroy();
		System.exit(0);
	}

	protected void loop(Context context) {
		scene.paint(context);
	}

	protected void init() {
		context = new Context(config);
		scene = new Scene();
		scene.init(context, sceneInitializer);
		updateCamera(context);
	}

	private void updateCamera(Context context) {
		Camera camera = context.getCameraManager().getActiveCamera();
		GL11.glLoadIdentity();
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GLU.gluLookAt(
				camera.eye.x, camera.eye.y, camera.eye.z,
				camera.center.x, camera.center.y, camera.center.z, 
				camera.up.x, camera.up.y, camera.up.z);
	}
	
	private void pollInput() {
		int dx = Mouse.getDX();
		int dy = Mouse.getDY();
		if(dx != 0 || dy != 0) {
			if(isInitial) {
				isInitial = false;
				return;
			}
			Camera camera = context.getCameraManager().getActiveCamera();
			camera.rotateYaw(dx/speedDivider);
			camera.rotatePitch(dy/speedDivider);
			updateCamera(context);
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
			Camera camera = context.getCameraManager().getActiveCamera();
			//Vec3f eye = camera.center.copy();
			camera.moveTF(-keyboardSpeed);
			//context.getScene().getLights().get(0).move(eye._minus(camera.eye));
			updateCamera(context);
		} else if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
			Camera camera = context.getCameraManager().getActiveCamera();
			//Vec3f eye = camera.center.copy();
			camera.moveTF(keyboardSpeed);
			//context.getScene().getLights().get(0).move(eye._minus(camera.eye));
			updateCamera(context);
		} else if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
			Camera camera = context.getCameraManager().getActiveCamera();
			//Vec3f eye = camera.center.copy();
			camera.moveLR(-keyboardSpeed);
			//context.getScene().getLights().get(0).move(eye._minus(camera.eye));
			updateCamera(context);
		} else if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
			Camera camera = context.getCameraManager().getActiveCamera();
			//Vec3f eye = camera.center.copy();
			camera.moveLR(keyboardSpeed);
			//context.getScene().getLights().get(0).move(eye._minus(camera.eye));
			updateCamera(context);
		}
	}
}