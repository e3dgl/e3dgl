package e3dgl.engine.console;

import e3dgl.engine.Context;
import e3dgl.engine.ContextProvider;
import e3dgl.engine.model.IModel;

public class Stats extends ContextProvider {

	private long startPaint = 0;

	private long endPaint;

	public Stats(Context context) {
		setContext(context);
	}

	public void beginPaint() {
		startPaint = System.currentTimeMillis();
	}

	public void endPaint() {
		endPaint = System.currentTimeMillis();
	}

	public int getFPS() {
		return (int) (1000. / (endPaint - startPaint));
	}

	public void paintModel(IModel paintedModel) {

	}

	public void endPaintModel(IModel paintedModel) {

	}

}
