package e3dgl.engine.camera;

import java.util.HashMap;
import java.util.Map;

import e3dgl.engine.Context;
import e3dgl.engine.ContextProvider;

public class CameraManager extends ContextProvider {

	private Map<Integer, Camera> cameras = new HashMap<Integer, Camera>();

	private int activeCameraId = -1;

	public CameraManager(Context context) {
		setContext(context);
	}

	public void createAndSetActiveCamera(float eyeX, float eyeY, float eyeZ,
			float centerX, float centerY, float centerZ, float upX, float upY,
			float upZ) {
		setActiveCamera(createCamera(eyeX, eyeY, eyeZ, centerX, centerY,
				centerZ, upX, upY, upZ));
	}

	public int getActiveCameraId() {
		return activeCameraId;
	}

	public Camera getActiveCamera() {
		return cameras.get(getActiveCameraId());
	}

	public void setActiveCamera(int cameraId) {
		activeCameraId = cameraId;
	}

	public void setActiveCamera(Camera camera) {
		setActiveCamera(camera.getId());
	}

	public Camera createCamera(float eyeX, float eyeY, float eyeZ,
			float centerX, float centerY, float centerZ, float upX, float upY,
			float upZ) {
		return addCamera(new Camera(getContext(), eyeX, eyeY, eyeZ, centerX,
				centerY, centerZ, upX, upY, upZ));
	}

	public Camera addCamera(Camera camera) {
		cameras.put(camera.getId(), camera);
		return camera;
	}

	public Camera getCamera(int cameraId) {
		return cameras.get(cameraId);
	}

	public Map<Integer, Camera> getCameras() {
		return cameras;
	}

}
