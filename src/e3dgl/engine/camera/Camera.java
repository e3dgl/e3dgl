package e3dgl.engine.camera;

import e3dgl.common.Utils;
import e3dgl.engine.Context;
import e3dgl.engine.ContextProvider;
import e3dgl.geometry.Vec3f;

public class Camera extends ContextProvider {

	private int id;

	public Vec3f eye;

	public Vec3f center;

	public Vec3f up;

	public Vec3f z;

	public Vec3f x;

	public Vec3f y;

	public Camera(Context context, Vec3f eye, Vec3f center, Vec3f up) {
		this(context, eye.x, eye.y, eye.z, center.x, center.y, center.z, up.x,
				up.y, up.z);
	}

	public void setEye(Vec3f eye) {
		setEye(eye.x, eye.y, eye.z);
	}

	public void setEye(float x, float y, float z) {
		eye.x = x;
		eye.y = y;
		eye.z = z;
		update();
	}

	public void setCenter(Vec3f center) {
		setCenter(center.x, center.y, center.z);
	}

	public void setCenter(float x, float y, float z) {
		center.x = x;
		center.y = y;
		center.z = z;
		update();
	}

	public void setUp(Vec3f up) {
		setUp(up.x, up.y, up.z);
	}

	public void setUp(float x, float y, float z) {
		up.x = x;
		up.y = y;
		up.z = z;
		update();
	}

	public Camera(int id, Context context) {
		this(id, context, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	}

	public Camera(Context context, float eyeX, float eyeY, float eyeZ,
			float centerX, float centerY, float centerZ, float upX, float upY,
			float upZ) {
		this(context.generateUniqueIdentifier(), context, eyeX, eyeY, eyeZ,
				centerX, centerY, centerZ, upX, upY, upZ);
	}

	public Camera(int id, Context context, float eyeX, float eyeY, float eyeZ,
			float centerX, float centerY, float centerZ, float upX, float upY,
			float upZ) {
		this.id = id;
		setContext(context);
		eye = new Vec3f(eyeX, eyeY, eyeZ);
		center = new Vec3f(centerX, centerY, centerZ);
		up = new Vec3f(upX, upY, upZ);
		update();
	}

	public int getId() {
		return id;
	}

	public void update() {
		z = eye.minus(center).normalize();
		x = up.crossProduct(z).normalize();
		y = z.crossProduct(x).normalize();
	}

	public void moveLR(float value) {
		Vec3f diff = x.mul(value);
		eye._plus(diff);
		center._plus(diff);
		update();
	}

	public void moveTF(float value) {
		Vec3f diff = z.mul(value);
		eye._plus(diff);
		center._plus(diff);
		update();
	}

	public void rotateYaw(float value) {
		center = Utils.randomAxisRotateMatrix(y, -value)
				.product(center._minus(eye)).toVector3fSingleton()._plus(eye);
		update();
	}

	public void rotatePitch(float value) {
		center = Utils.randomAxisRotateMatrix(x, value)
				.product(center._minus(eye)).toVector3fSingleton()._plus(eye);
		update();
	}

	public String toString() {
		return "eye(" + eye + "), center(" + center + "), up(" + up + ")";
	}
}
