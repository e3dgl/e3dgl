package e3dgl.engine.exceptions;

public class UnimplementedYet extends UnsupportedOperationException {

	private static final long serialVersionUID = 1L;

	public UnimplementedYet() {
		super("Unimplemented yet!");
	}

}
