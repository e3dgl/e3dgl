package e3dgl.engine.shader;

import org.lwjgl.opengl.ARBShaderObjects;

public class DefaultShader extends AbstractShader {

	public DefaultShader() {
		super(DefaultShader.class.getSimpleName());
	}
	
	@Override
	public void link() {
	}

	@Override
	public void use() {
		ARBShaderObjects.glUseProgramObjectARB(0);
	}

	@Override
	public void unuse() {
		ARBShaderObjects.glUseProgramObjectARB(0);
	}
}
