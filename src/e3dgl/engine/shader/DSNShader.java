package e3dgl.engine.shader;

import org.lwjgl.opengl.GL20;

import e3dgl.geometry.Vec3f;

public class DSNShader extends AbstractShader {

	public DSNShader() {
		super(DSNShader.class.getSimpleName());
	}
	
	private float timer = 1.0f;
	
	private float timerMod = 0.000015f;

	@Override
	public void preprocess() {
		
		if(timer >= 1.01f || timer < 1.0f)
			timerMod = -timerMod;
		timer += timerMod;
		int scaleId = GL20.glGetUniformLocation(getShaderProgramId(), "scale");
		GL20.glUniform4f(scaleId, timer, timer, timer, timer);
		
		int textureId = GL20.glGetUniformLocation(getShaderProgramId(), "u_texture");
		GL20.glUniform1i(textureId, 0);
		
		int normalsId = GL20.glGetUniformLocation(getShaderProgramId(), "u_normals");
		GL20.glUniform1i(normalsId, 1);
		
		int specularId = GL20.glGetUniformLocation(getShaderProgramId(), "u_specular");
		GL20.glUniform1i(specularId, 2);
		
		/*Vec3f cameraPosition = getContext().getCameraManager().getActiveCamera().center;
		int cameraPositionId = GL20.glGetUniformLocation(getShaderProgramId(), "CAMERA_POSITION");
		GL20.glUniform3f(cameraPositionId, cameraPosition.x, cameraPosition.y, cameraPosition.z);*/
		
		Vec3f cameraEye = getContext().getCameraManager().getActiveCamera().eye;
		int cameraEyeId = GL20.glGetUniformLocation(getShaderProgramId(), "CAMERA_POSITION");
		GL20.glUniform3f(cameraEyeId, cameraEye.x, cameraEye.y, cameraEye.z);				

	}

}
