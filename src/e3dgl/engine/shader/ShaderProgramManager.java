package e3dgl.engine.shader;

import java.util.HashMap;
import java.util.Map;

import e3dgl.engine.Context;
import e3dgl.engine.ContextProvider;

public class ShaderProgramManager extends ContextProvider {

	public static final String SHADER_ROLE_DEFAULT = "Default shader role";

	private Map<String, IShaderProgram> shaders = new HashMap<String, IShaderProgram>();

	private Map<String, IShaderProgram> shadersByRole = new HashMap<String, IShaderProgram>();

	public ShaderProgramManager(Context context) {
		setContext(context);
		IShaderProgram shaderProgram = new DefaultShader();
		shaderProgram.setContext(context);
		addShaderProgramByRole(SHADER_ROLE_DEFAULT, new DefaultShader());
	}

	public void addShaderProgram(IShaderProgram shader) {
		shaders.put(shader.getName(), shader);
		shader.link();
	}

	public void addShaderProgramByRole(String role, IShaderProgram shader) {
		shadersByRole.put(role, shader);
	}

	public IShaderProgram getDefaultShaderProgram() {
		return shadersByRole.get(SHADER_ROLE_DEFAULT);
	}

	public IShaderProgram getShaderProgram(String shaderName) {
		return shaders.get(shaderName);
	}

	public Map<String, IShaderProgram> getShaderPrograms() {
		return shaders;
	}

	public IShaderProgram load(Context context, String shaderName) {
		String shaderClassName = getClass().getPackage().getName() + "." + shaderName;  
		try {
			IShaderProgram shaderProgram = (IShaderProgram)getClass().getClassLoader().loadClass(shaderClassName).newInstance();
			shaderProgram.setContext(context);
			return shaderProgram;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

}
