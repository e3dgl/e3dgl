package e3dgl.engine.shader;

import java.util.List;

import e3dgl.engine.IContextProvider;
import e3dgl.engine.material.IMaterial;
import e3dgl.engine.model.Color4f;
import e3dgl.engine.model.light.ILight;
import e3dgl.engine.texture.ITexture;

public interface IShaderProgram extends IContextProvider {

	public String getName();

	public void setFragmentShaderName(String name);

	public void setVertexShaderName(String name);

	public void setDiffuseMap(ITexture texture);

	public void setNormalMap(ITexture texture);

	public void setSpecMap(ITexture texture);

	public void setMaterial(IMaterial material);

	public String getFragmentShaderName();

	public String getVertexShaderName();

	public void setLights(List<ILight> lights);

	public void link();

	public void use();

	public void unuse();

	public void setColor(Color4f color);

	public Color4f getColor();

	public int getShaderProgramId();

	public int getTangentIndex();

	public int getBiTangentIndex();

}
