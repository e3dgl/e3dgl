package e3dgl.engine.shader;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import e3dgl.common.Utils;
import e3dgl.engine.Context;
import e3dgl.engine.ContextProvider;
import e3dgl.engine.material.IMaterial;
import e3dgl.engine.model.Color4f;
import e3dgl.engine.model.light.ILight;
import e3dgl.engine.texture.ITexture;

public abstract class AbstractShader extends ContextProvider implements
		IShaderProgram {

	private String name;

	private String fragmentShaderName;

	private String vertexShaderName;

	protected ITexture diffuseMap;

	protected ITexture normalMap;

	protected ITexture specMap;

	protected IMaterial material;

	private int vertexShaderId = -1;

	private int fragmentShaderId = -1;

	private int shaderProgramId = -1;
	
	private int tangentId = 1;
	
	private int biTangentId = 2;

	private List<ILight> lights = new ArrayList<ILight>();

	private Color4f color = new Color4f();

	public AbstractShader(String name) {
		this.name = name;
	}

	public AbstractShader(Context context, String name) {
		this(name);
		setContext(context);
	}

	public IMaterial getMaterial() {
		return material;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setDiffuseMap(ITexture texture) {
		this.diffuseMap = texture;
	}

	@Override
	public void setNormalMap(ITexture texture) {
		this.normalMap = texture;
	}

	@Override
	public void setSpecMap(ITexture texture) {
		this.specMap = texture;
	}

	@Override
	public void setMaterial(IMaterial material) {
		this.material = material;
	}

	public String getFragmentShaderName() {
		return fragmentShaderName;
	}

	@Override
	public void setFragmentShaderName(String fragmentShaderName) {
		this.fragmentShaderName = fragmentShaderName;
	}

	public String getVertexShaderName() {
		return vertexShaderName;
	}

	@Override
	public void setVertexShaderName(String vertexShaderName) {
		this.vertexShaderName = vertexShaderName;

	}

	protected void createProgram() {

	}

	@Override
	public void link() {
		vertexShaderId = Utils.createShader(vertexShaderName, false);
		fragmentShaderId = Utils.createShader(fragmentShaderName, true);
		shaderProgramId = GL20.glCreateProgram();
		GL20.glAttachShader(shaderProgramId, vertexShaderId);
		GL20.glAttachShader(shaderProgramId, fragmentShaderId);
		GL20.glLinkProgram(shaderProgramId);
		int comp = GL20.glGetProgrami(shaderProgramId, GL20.GL_LINK_STATUS);
		int len = GL20.glGetProgrami(shaderProgramId, GL20.GL_INFO_LOG_LENGTH);
		String err = GL20.glGetProgramInfoLog(shaderProgramId, len);
		String log = "";
		if (err != null && err.length() != 0)
			log = err + "\n" + log;
		if (log != null)
			log = log.trim();
		if (comp == GL11.GL_FALSE)
			try {
				throw new LWJGLException(log.length() != 0 ? log
						: "Could not link program");
			} catch (LWJGLException e) {
				e.printStackTrace();
			}
		GL20.glBindAttribLocation(getShaderProgramId(), tangentId, "tangent");
		GL20.glBindAttribLocation(getShaderProgramId(), biTangentId, "bitangent");
	}

	@Override
	public void use() {
		ARBShaderObjects.glUseProgramObjectARB(shaderProgramId);
		preprocess();
	}

	protected void preprocess() {
	}

	@Override
	public void unuse() {
		ARBShaderObjects.glUseProgramObjectARB(0);
	}

	@Override
	public int getShaderProgramId() {
		return shaderProgramId;
	}

	@Override
	public void setLights(List<ILight> lights) {
		this.lights.clear();
		this.lights.addAll(lights);
	}

	public List<ILight> getLights() {
		return lights;
	}

	public Color4f getColor() {
		return color;
	}

	public void setColor(Color4f color) {
		setColor(color.r, color.g, color.b, color.a);
	}

	public void setColor(float r, float g, float b, float a) {
		color.r = r;
		color.g = g;
		color.b = b;
		color.a = a;
	}

	@Override
	public int getTangentIndex() {
		return tangentId;
	}

	@Override
	public int getBiTangentIndex() {
		return biTangentId;
	}

}
